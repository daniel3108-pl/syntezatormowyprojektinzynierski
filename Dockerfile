FROM pytorch/pytorch:1.13.0-cuda11.6-cudnn8-devel

ENV NOTEBOOK_ACCESS_TOKEN=6c4bfd9ffd59618566557195d0f0a733f246545b8b118ca7

WORKDIR /speechsynthesizer

RUN apt-get update -y
RUN apt-get install git -y
RUN apt-get install libsndfile1-dev -y
RUN pip install pillow jupyter sox PySoundFile
RUN pip install torchaudio===0.13.0+cu116 -f https://download.pytorch.org/whl/cu116/torch_stable.html

RUN pip install -U git+https://gitlab.com/daniel3108-pl/syntezatormowyprojektinzynierski.git@master

RUN apt-get install curl -y
RUN curl https://gitlab.com/daniel3108-pl/syntezatormowyprojektinzynierski/-/raw/main/speechsynthesizer/utils/logger/logger.cfg --output /opt/conda/lib/python3.9/site-packages/speechsynthesizer/utils/logger/logger.cfg

EXPOSE 8888
CMD jupyter notebook --no-browser --ip=0.0.0.0 --port=8888 --NotebookApp.token=$NOTEBOOK_ACCESS_TOKEN --allow-root
