#!/bin/zsh -i

# Ten skrypt działa tylko w przypadku gdy na komputerze zainstalowana jest dystrybucja python - Anaconda
# Dla pełnej kompatybilności zalecane jest użycie tej dystrybucji wraz z tą komendą, która tworzy automatycznie
# wirtualne środowisko dla projektu, projekt potem uruchamiamy z tego środowiska "speechsynthesizer"

# Skrypt powinien być kompatybilny z systemami linux i macOS, w przypadku Windows należy skorzystać ze skryptu
# "installenv.bat" lub ręcznie komenda po komendzie wpisywać w terminal Anaconda.

# Przed uruchomieniem skryptu należy wykonać komendę: `conda init NAZWA_POWŁOKI` np. `conda init zsh`, pozwala ona użyć
# komendy conda activate, po wykonaniu conda init nalezy zamknac wszystkie okna terminala

# skrypt uruchamiamy komendą odpowiedniego interpretera, np. `zsh -i installenv.sh`, inaczej nie zadziała

ENV_NAME="speechsynthesizer"
PY_VERSION="3.9"

conda create -n $ENV_NAME python=$PY_VERSION -y
conda activate $ENV_NAME
conda install -c pytorch torchaudio -y
conda install -c conda-forge librosa -y
conda install -c pytorch torchtext -y
conda install -c anaconda pandas -y
conda install matplotlib -y
conda install -c conda-forge pyyaml -y
conda install -c conda-forge parameterized -y
conda install -c conda-forge fpdf2 -y
conda install -c conda-forge pydantic -y
pip install pdoc
pip install coverage
