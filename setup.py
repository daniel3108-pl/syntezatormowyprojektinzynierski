from setuptools import setup, find_packages

setup(
    name="Speech synthesizer",
    version="1.2.2",
    author="Daniel Świetlik",
    description="Trains speech generating neural network model "
                "and allows to use that model to generate speech audio file",
    entry_points={
        'console_scripts': [
            'speechsynthesizer = speechsynthesizer.main:main'
        ],
    },
    packages=find_packages(exclude=["test*"]),
    package_data={
        '': ['*.cfg']
    },
    include_package_data=True,
    install_requires=[
        'torchaudio',
        'intel-openmp',
        'torchaudio',
        'librosa',
        'pandas',
        'matplotlib',
        'pyyaml',
        'fpdf2',
        'pydantic',
        'noisereduce'
    ],
    python_requires=">=3.9",
    classifiers=[
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
        "Operating System :: MacOS :: MacOS X",
        "Operating System :: POSIX",
        "Operating System :: Microsoft :: Windows",
    ],
    platforms=[
        "posix",
        "nt"
    ]
)

