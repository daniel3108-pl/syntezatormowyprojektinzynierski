"""
# Daniel Świetlik's Text-To-Speech system - Documentation 

Documentation for text-to-speech system based on Tacotron 2 neural netowork algorithm. It presents all classes and methods used in this project. It allows to display their source code by clicking `View Source` button.

Submodules section on the left lists all modules in the project. Clicking each one allows to go to page to see content 
for the specific module.

## Requirements for running app
Have **python 3.9** or newer installed, preferred [**anaconda**](https://www.anaconda.com) distribution

Install all necessary libraries used by the app with requirements.txt file
```
python3 -m pip install -r requirements.txt
```

The easiest way to install it would be to use anaconda and one of provided scripts 
that creates a new environment for the project automatically and installs all dependencies.
Use `installenv.sh` or `installenv.ps1` depending on the os you are using, this will ensure 
having exact same development environment.  You can also use Docker with provided Dockerfile.

## Installing application using pip

For installing application with pip and then running it you need to use this
command (It is suggested to create a virtual environment 
as this would install lots of dependencies):
```bash
pip install -U git+https://github.com/daniel3108-pl/TextToSpeechSystem_WaveNetBased.git@master
```

After installing it you can use `speechsynthesizer` command to run the application
with the same arguments as stated above.

## How to run application

### Training the model
```bash
python -m speechsynthesizer.main train [--config<config_path>] [--mode(gpu|cpu)] [--report<report_output_path>]
```

Application also allows to generate pdf report of training process, 
but you have to have Times New Roman font in your system font directory. 
Look for `report_printer.py` module and `_get_font()` function to see what directories are scanned.

### Additional configs

If you want to have more indepth access to config param you have
to create `params.py` module inside your current working directory
from where you're going to train your model. 
In that file you can overwrite any parameter that can be found in 
`speechsynthesizer/model/utils/params.py` module. Please remember
not to run any code from it outside of variable assignment as it may
break the program.

### Generating audio from text
```bash
python -m speechsynthesizer.main generate [--text<text>] [--out<audio_out_path>]
```

For providing text there are 4 ways:

- using `--text` option from command e.g. 
    ```bash
    python -m speechsynthesizer.main generate --text "some speech text" ...rest
    ```
- inputing from console after running the app, app will ask for input
- using terminal pipe functionality e.g.
    ```bash
    cat some_file_with_text.txt | python -m speechsynthesizer.main ...rest
    ```
- providing text file in text argument
    ```bash
    python -m speechsynthesizer.main generate --text ./text_file.txt ...rest
    ```

### Example of usage after installing via PIP

```bash
speechsynthesizer train [--config<config_path>]
```

### Using Dockerfile

Provided Dockerfile allows to create an image with application and jupyter notebook server. Docker image exposes jupyter on port `8888` and uses docker's environmental variable `NOTEBOOK_ACCESS_TOKEN` to set server's authentication token for loging in.

#### Running app with docker container

```bash
docker run --name speechsyntehsizer -d -p 8888:8888 -v ./data:/speechsynthesizer/data -e NOTEBOOK_ACCESS_TOKEN=1234 speechsynthesizer
```

After running it you can use container's jupyter notebook server at
`localhost:8888`.
"""
