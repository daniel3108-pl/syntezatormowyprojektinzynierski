"""Module with function that creates ArgumentParser object that parses user arguments"""

import argparse


def get_argument_parser() -> argparse.ArgumentParser:
    """Creates argument parser for user input for train module and generation module

    Returns:
        ArgumentParser object with configuration for application input arguments
    """
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(
        help="Choose program functionality [training model | generating audio from text], \n"
             "You can use [--help | -h] with each command to see its arguments", dest='func')
    _prepare_train_parser(subparsers)
    _prepare_generate_parser(subparsers)
    return parser


def _prepare_train_parser(subparsers):
    """Initialize parser object to parse training process arguments

    Args:
        subparsers: Subparser object to initialize
    """
    train_parser = subparsers.add_parser("train", help="using it will make program train your model")
    train_parser.add_argument("--config", "-c", help="set optional config file for training process",
                              required=False,
                              dest="config")
    train_parser.add_argument("--report", "-r", help="provide a path to create training report",
                              required=False, dest="report")
    train_parser.add_argument("--mode", "-m", help="Mode of training to process either on 'gpu' or cpu",
                              choices=("gpu", "cpu"), default="cpu", dest="mode")


def _prepare_generate_parser(subparsers):
    """Initialize parser object to parse generation process arguments

    Args:
        subparsers: Subparser object
    """
    generate_parser = subparsers.add_parser("generate",
                                            help="using it will make program generate audio based on your input")
    required_group = generate_parser.add_argument_group("required arguments")
    required_group.add_argument("--model", "-m", help="path to the trained model", required=True, dest="model")

    optional_group = generate_parser.add_argument_group("optional arguments")
    optional_group.add_argument("--out", "-o", help="name of the output file without extension", required=False,
                                dest="out_file")
    optional_group.add_argument("--text", "-t", help="text to generate audio from", required=False, dest="text")
    optional_group.add_argument("--sample-rate", "-sr", help="sample rate for speech audio to generate",
                                dest="sample_rate", type=int)