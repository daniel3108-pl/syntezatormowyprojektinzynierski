"""Module with audio generator class"""

import os
from logging import Logger
from typing import Optional, Tuple

import torch
import matplotlib.pyplot as plt

from .model.model import MelPredictionNetwork
from .model.utils.mel import MelToWaveConverter, MelWaveFileWriter
from .model.utils.text import TextEncoder
from .utils import logger, func


class AudioGenerator:
    """Takes care of a process for generating wave files using user provided model
    and saving results to wave file.
    """

    def __init__(self, model: str, text: str, output_path: Optional[str], rate: Optional[int]):
        """Generates wave files for speeches with pretrained model based on user input.

        Args:
            model: path to binary model file
            text: speech transcription
            output_path: path for output audio file
            rate: sample rate for audio file
        """
        self.log: Logger = logger.get_logger(self.__class__.__name__)
        self.text: str = self._load_text(text)
        self.output_path = output_path or "./synthesized_speech"
        self.model_path = model
        self.sample_rate = rate or 22050

    def generate(self):
        """Runs speech generation process and then saves mel prediction into a new wave file
        """
        if not self._is_input_valid():
            return
        sample_rate, generated_mel = self._make_prediction()
        self._convert_prediction_to_wave_file(generated_mel, sample_rate)

    @classmethod
    def _load_text(cls, text: str) -> str:
        """Loads text from file with provided path or returns argument as text

        Args:
            text: text to return of file path to load

        Returns:
            String object with speech text
        """
        if func.file_exists(text):
            return cls._read_text_from_file(text)
        return text

    @staticmethod
    def _read_text_from_file(text_path: str) -> str:
        """Reads text for text file

        Args:
            text_path: path to file

        Returns:
            String object with text
        """
        with open(text_path, "r") as text_file:
            return " ".join(text_file.readlines())

    def _is_input_valid(self):
        """Validates users provided arguments

        Returns:
            True if args are valid False if not
        """
        valid = True
        if not func.file_exists(self.model_path):
            self.log.warning("Model file is not correct, please provide proper model file")
            valid = False
        if func.is_text_empty(self.text):
            self.log.warning("Your input speech is incorrect. Please provided some text.")
            valid = False
        if func.is_text_empty(self.output_path):
            self.log.warning("You haven't specified correct output path, please do so")
            valid = False
        return valid

    def _make_prediction(self) -> Tuple[int, torch.Tensor]:
        """Generates audio from user provided text

        Returns:
            Tuple of sample rate and mel spectrogram predicted from the model
        """
        model, _ = MelPredictionNetwork.load(self.model_path)
        model.eval()
        encoded_text: torch.Tensor = self._encode_input(self.text)
        return self._predict_mel_from_text(encoded_text, model.eval())

    def _encode_input(self, text: str) -> torch.Tensor:
        """Encodes user's speech text to one hot vector

        Args:
            text: text to encode

        Returns:
            Tensor with text's one hot vector
        """
        encoder = TextEncoder()
        encoded_text: torch.Tensor = encoder.encode(text)
        self.log.info("Encoded user input text.")
        return torch.autograd.Variable(encoded_text).long()

    def _predict_mel_from_text(self, encoded_text: torch.Tensor, model: MelPredictionNetwork) \
            -> Tuple[int, torch.Tensor]:
        """Generates mel spectrogram with speech with loaded model

        Args:
            encoded_text: encoded text tensor
            model: neural network model object

        Returns:
            Tuple of integer sample rate and mel spectrogram Tensor
        """
        self.log.info(f"Started predicting audio from text: {self.text}...")
        prediction_mel, prediction_post_mel, alignments = model.predict(encoded_text.unsqueeze(0))
        self.log.info("Successfully predicted speech audio")
        return self.sample_rate, prediction_post_mel.float().data

    def _convert_prediction_to_wave_file(self, generated_mel: torch.Tensor, sample_rate: int):
        """Takes mel spectrogram tensor and creates wave file using it.

        Args:
            generated_mel: speech's mel spectrogram tensor
            sample_rate: sample rate integer
        """
        converter = MelToWaveConverter()
        waveform: torch.Tensor = converter.convert(sample_rate, generated_mel)
        writer = MelWaveFileWriter()
        writer.write(self.output_path, sample_rate, waveform.data)


def _plot_data(mel_spectrogram: torch.Tensor, mel_alignments: torch.Tensor):
    """For model debugging process
    """
    fig, axes = plt.subplots(1, 2, figsize=(16, 4))
    axes[0].set_title("Mel spectrogram")
    axes[0].imshow(mel_spectrogram.float().data[0], aspect='auto', origin='lower',
                   interpolation='none')
    axes[1].set_title("Mel alignments")
    axes[1].imshow(mel_alignments.float().data[0].T, aspect='auto', origin='lower',
                   interpolation='none')
    plt.show()
