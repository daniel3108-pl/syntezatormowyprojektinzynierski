"""Application main module with entry point for it"""

import argparse

from .argparser import get_argument_parser
from .generator import AudioGenerator
from .trainer import TextToSpeechTrainer
from .utils.logger import get_logger


def main():
    """Function that parses arguments and runs selected application module (train or generate)
    """
    console_args = get_argument_parser().parse_args()
    log = get_logger(__name__)
    log.info("Application has started...")
    if console_args.func == "train":
        _train_model(console_args)
    elif console_args.func == "generate":
        _generate_speech(console_args)


def _train_model(args: argparse.Namespace):
    """Starts training process of application with provided arguments

    Args:
        args: object that stores all arguments provided
    """
    trainer = TextToSpeechTrainer(args.config, args.report, args.mode)
    trainer.train()


def _generate_speech(args: argparse.Namespace):
    """Starts speech generation process with provided arguments

    Args:
        args:
    """
    text = args.text or input("Text to generate from: \n")
    generator = AudioGenerator(args.model, text, args.out_file, args.sample_rate)
    generator.generate()


if __name__ == "__main__":
    main()
