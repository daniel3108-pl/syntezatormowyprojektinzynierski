"""Module that stores class for calculation location aware attention"""
from typing import Optional, Tuple

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import Tensor
from torch.autograd import Variable

from .utils import params, func


class LocalAttention(nn.Module):
    """Location aware attention model implementation
    from arXiv:1506.07503v1 publication.
    """

    def __init__(self):
        """Initializes attention layer object
        """
        super(LocalAttention, self).__init__()
        self.weights: Optional[Variable] = None
        self.weights_cumulative: Optional[Variable] = None

        self.query_layer: nn.Linear = func.xavier_init_layer(nn.Linear, "tanh", params.attention_rnn_dim,
                                                             params.attention_hid_dim, bias=False)
        self.value_layer: nn.Linear = func.xavier_init_layer(nn.Linear, "tanh", params.emb_dim,
                                                             params.attention_hid_dim, bias=False)
        self.score_layer: nn.Linear = func.xavier_init_layer(nn.Linear, "linear", params.attention_hid_dim,
                                                             1, bias=True)
        self.local_conv: nn.Conv1d = self._get_convolution()
        self.local_conv_proj: nn.Linear = func.xavier_init_layer(nn.Linear, "tanh", params.attention_n_filters,
                                                                 params.attention_hid_dim, bias=False)

    def prepare(self, n_batch: int, max_time: int, encoder_state: Tensor):
        """Initializes weights and cumulative weights of attention for learning process

        Args:
            n_batch: number of batches
            max_time: number of mel bins
            encoder_state: encoder output tensor
        """
        self.weights = func.new_tensor_variable((n_batch, max_time), encoder_state)
        self.weights_cumulative = func.new_tensor_variable((n_batch, max_time), encoder_state)

    def forward(self, query: Tensor, value: Tensor, processed_value: Tensor,
                mask: Optional[Tensor]) -> Tuple[Tensor, Tensor]:
        """Does single iteration of learning a batch of encoded text data and mel spectrogram

        Args:
            query: decoder mel input data
            value: encoder speech text output
            processed_value: process by the value_layer encoder speech text output
            mask: mask for padded data if None that means no masking will be applied

        Returns:
            attention data and weights
        """
        previous_attention = func.cat_tensor_unsqueeze(self.weights, self.weights_cumulative)
        energies = self._get_energies(query, processed_value, previous_attention)
        if mask is not None:
            energies.data.masked_fill_(mask, -float("inf"))
        context, weights = self._get_attentions(energies, value)
        self.weights_cumulative += self.weights
        return context, weights

    @staticmethod
    def _get_convolution() -> nn.Conv1d:
        """Creates convolution layer object

        Returns:
            nn.Conv1d object
        """
        kernel_size = params.attention_loc_kernel_size
        return func.xavier_init_layer(
            nn.Conv1d, "linear",
            in_channels=2,
            out_channels=params.attention_n_filters,
            kernel_size=kernel_size,
            padding=int((kernel_size - 1) / 2),
            stride=1, dilation=1, bias=False
        )

    def _get_energies(self, query: Tensor, proc_value: Tensor, attention: Tensor) -> Tensor:
        """Calculates attention energies

        Args:
            query: query object - decoder state
            proc_value: processed value - encoder output states
            attention: previous iteration attention weights

        Returns:
            Energies tensor
        """
        proc_query = self.query_layer(query.unsqueeze(1))
        proc_attention = self._process_attention(attention)
        energies = self.score_layer(torch.tanh(proc_query + proc_attention + proc_value))
        return energies.squeeze(-1)

    def _process_attention(self, attention: Tensor) -> Tensor:
        """Processes attention weights with convolution for energies

        Args:
            attention: attention weights tensor

        Returns:
            Tensor of process attention weights
        """
        proc_attention = self.local_conv(attention)
        proc_attention = proc_attention.transpose(1, 2)
        proc_attention = self.local_conv_proj(proc_attention)
        return proc_attention

    @staticmethod
    def _get_attentions(energies: Tensor, value: Tensor) -> Tuple[Tensor, Tensor]:
        """Calculates location aware attention weights and context

        Args:
            energies: attention energies
            value: encoder outputs

        Returns:
            Tuple of attention context tensor and attention weights tensor
        """
        attention_weights = F.softmax(energies, dim=1)
        attention_context = torch.bmm(attention_weights.unsqueeze(1), value)
        return attention_context.squeeze(1), attention_weights
