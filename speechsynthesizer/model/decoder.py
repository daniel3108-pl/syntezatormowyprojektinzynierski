"""Module that stores classes related to decoder from Tacotron model"""
from typing import Tuple, List, Optional

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import Tensor

from .utils import params
from .utils import func
from .utils.layers import BatchNormalizedConv1d
from .attention import LocalAttention


class DecoderPreNet(nn.Module):
    """Decoder's PreMet object
    """

    def __init__(self):
        """Initializes linear layers for the network
        """
        super(DecoderPreNet, self).__init__()
        self.layers: nn.ModuleList = self._create_layers()

    def forward(self, mel: Tensor) -> Tensor:
        """Does single iteration on input data through its linear layers

        Args:
            mel: mel spectrogram tensor

        Returns:
            processed mel spectrogram tensor
        """
        for layer in self.layers:
            mel = F.dropout(F.relu(layer(mel)), params.decoder_dropout, True)
        return mel

    @staticmethod
    def _create_layers() -> nn.ModuleList:
        """Creates linear layers for the network

        Returns:
            ModuleList of linear layers
        """
        return nn.ModuleList([
            func.xavier_init_layer(nn.Linear, "linear",
                                   params.n_mels,
                                   params.decoder_prenet_dim, bias=False),
            func.xavier_init_layer(nn.Linear, "linear",
                                   params.decoder_prenet_dim,
                                   params.decoder_prenet_dim, bias=False),
        ])


class DecoderPostNet(nn.Module):
    """Postnet for decoder that processes mel spectrogram
    """

    def __init__(self):
        """Initializes object with convolution layers and dropout layer
        """
        super(DecoderPostNet, self).__init__()
        self.convolutions: nn.ModuleList = self._create_convolutions()
        self.dropout = nn.Dropout(params.dropout_prob)

    def forward(self, mel: Tensor) -> Tensor:
        """Does one iteration of batch processing

        Args:
            mel: predicted mel spectrogram from attention

        Returns:
            processed mel spectrogram
        """
        for conv in self.convolutions:
            mel = self.dropout(conv(mel))
        return mel

    @staticmethod
    def _create_convolutions() -> nn.ModuleList:
        """Creates a list of convolution layers for the network

        Returns:
            ModuleList of BatchNormalizedConv1d objects
        """
        layers_left = params.decoder_postnet_n_conv_layers - 2
        return nn.ModuleList([
            BatchNormalizedConv1d(params.n_mels, params.emb_dim, params.emb_dim, activation="tanh", init_gain="tanh"),
            # creates 3 of the same convolutions and unpacks it so that the final list would have 5 layers
            *[BatchNormalizedConv1d(params.emb_dim, activation="tanh", init_gain="tanh")
              for _ in range(layers_left)],
            BatchNormalizedConv1d(params.emb_dim, params.n_mels, params.n_mels, activation=""),
        ])


class DecoderNetwork(nn.Module):
    """Class for decoder part of tacotron architecture, it takes mel spectrograms as well
    as encoder outputs and then learns how to translate transcription into a mel representation
    """

    def __init__(self):
        """Initializes decoder network object with params and necessary layers
        for it to works
        """
        super(DecoderNetwork, self).__init__()
        self.mask_encoder_out: Optional[torch.LongTensor] = None
        self.prenet = DecoderPreNet()
        self.lstm_cells: nn.ModuleList = self._create_lstm_cells()
        self.attention = LocalAttention()
        self.post_net = DecoderPostNet()
        self.linear_proj: nn.Linear = func.xavier_init_layer(nn.Linear, "linear",
                                                             params.attention_rnn_dim + params.emb_dim,
                                                             params.n_mels)
        self.eos_proj: nn.Linear = func.xavier_init_layer(nn.Linear, "sigmoid",
                                                          params.decoder_eos_dim, 1)
        self.dropout = nn.Dropout(params.decoder_dropout)
        self.attention_dropout = nn.Dropout(params.attention_dropout)

    def forward(self, encoded_text: Tensor, mel: Tensor, text_sizes: Tensor) \
            -> Tuple[Tensor, Tensor, Tensor, Tensor]:
        """Does one iteration of learning with an encoded text (outputs from the encoder) and mel spectrograms,
        processing them through all decoder layers

        Args:
            encoded_text: encoder output (hidden representation of transcription)
            mel: mel spectrogram data from batch
            text_sizes: sizes of encoder input

        Returns:
            output of the network (and also the entire model) - mel prediction, mel prediction after postnet, and
            attention alignment
        """
        self._prepare_decoder(encoded_text, text_sizes)
        mel_inputs = self._prepare_mel_for_prenet(mel, encoded_text)
        mel_inputs = self.prenet(mel_inputs)
        mels, alignments, eoss = self._do_decoding_loop(mel_inputs)
        mels, alignments, eoss = self._prepare_for_postnet(mels, alignments, eoss)
        mels_post = mels + self.post_net(mels)
        return mels, mels_post, alignments, eoss

    @torch.no_grad()
    def predict(self, encoded_text: Tensor) -> Tuple[Tensor, ...]:
        """Takes encoder outputs and translates them into a mel spectrogram for prediction of
        an audio data.

        Args:
            encoded_text: encoder output, hidden representation of speech to be predicted

        Returns:
            mel prediction after postnet
        """
        self._prepare_decoder_and_attention_states(encoded_text)
        n_batch = encoded_text.size(0)
        mel_input = func.new_tensor_variable((n_batch, params.n_mels), encoded_text)
        alignments, mels, eoss = self._do_prediction_decoding_loop(mel_input)
        mels, alignments, eoss = self._prepare_for_postnet(mels, alignments, eoss)
        mels_post = mels + self.post_net(mels)
        return mels, mels_post, alignments

    @property
    def device(self) -> torch.device:
        """Returns device that model is on

        Returns:
            torch.Device object
        """
        return next(self.parameters()).device

    @staticmethod
    def _create_lstm_cells() -> nn.ModuleList:
        """Creates lstm cells module list

        Returns:
            Module list of lstm cells
        """
        return nn.ModuleList([
            nn.LSTMCell(params.decoder_prenet_dim + params.emb_dim, params.decoder_n_lstm_units, 1),
            nn.LSTMCell(params.attention_rnn_dim + params.emb_dim, params.decoder_n_lstm_units, 1),
        ])

    def _prepare_decoder(self, encoded_text: Tensor, text_sizes: Tensor):
        """Initializes decoder network states tensors

        Args:
            encoded_text: encoder network outputs
            text_sizes: original sizes of transcriptions tensors
        """
        self.mask_encoder_out = func.get_mask(text_sizes, device=self.device)
        self._prepare_decoder_and_attention_states(encoded_text)

    def _prepare_decoder_and_attention_states(self, encoder_state: Tensor):
        """Initializes decoder network states and attention network states

        Args:
            encoder_state: encoder network outputs
        """
        n_batch = encoder_state.size(0)
        max_time = encoder_state.size(1)
        self._prepare_decoder_states(encoder_state, n_batch)
        self._prepare_attention_states(encoder_state, max_time, n_batch)

    def _prepare_decoder_states(self, encoder_state: Tensor, n_batch: int):
        """Initializes decoder network states

        Args:
            encoder_state: encoder network outputs
            n_batch: size of batch
        """
        self.encoder_state = encoder_state
        self.mel_hid = func.new_tensor_variable((n_batch, params.attention_rnn_dim), encoder_state)
        self.mel_cell = func.new_tensor_variable((n_batch, params.attention_rnn_dim), encoder_state)

    def _prepare_attention_states(self, encoder_state: Tensor, max_time: int, n_batch: int):
        """Initializes attention related states

        Args:
            encoder_state: encoder network outputs
            max_time: mel spectrogram's number segments
            n_batch: size of batch
        """
        self.attention.prepare(n_batch, max_time, encoder_state)
        self.attn_hid = func.new_tensor_variable((n_batch, params.attention_rnn_dim), encoder_state)
        self.attn_cell = func.new_tensor_variable((n_batch, params.attention_rnn_dim), encoder_state)
        self.attn_ctx = func.new_tensor_variable((n_batch, params.emb_dim), encoder_state)
        self.attn_processed_encoder_state = self.attention.value_layer(self.encoder_state)

    def _prepare_mel_for_prenet(self, mels: Tensor, encoded_text: Tensor) -> Tensor:
        """Preprocesses mel spectrograms tensors for PreNet network

        Args:
            mels: mel spectrograms
            encoded_text: encoder outputs

        Returns:
            Preprocessed mel spectrograms tensors
        """
        n_batch = mels.size(0)
        mel_first_ = func.new_tensor_variable((n_batch, params.n_mels), encoded_text).unsqueeze(0)
        mels = mels.transpose(1, 2)
        mels = mels.view(mels.size(0), mels.size(1), -1)
        mels = mels.transpose(0, 1)
        return torch.cat((mel_first_, mels), dim=0)

    def _do_decoding_loop(self, mel_inputs: Tensor) -> Tuple[Tensor, ...]:
        """Iterates over mel spectrogram bins and generates a output mel spectrogram with teacher forcing

        Args:
            mel_inputs: original mel spectrograms for teacher forcing

        Returns:
            Tuple of generated mel spectrogram bins, attention weights, and eos tensor
        """
        mels, alignments, eoss = [], [], []
        for i in range(0, mel_inputs.size(0) - 1):
            mel_outputs, alignment, eos = self._decode_to_mel(mel_inputs[i])
            mels.append(mel_outputs.squeeze(1))
            alignments.append(alignment)
            eoss.append(eos.squeeze(1))
        return (
            torch.stack(mels),
            torch.stack(alignments),
            torch.stack(eoss)
        )

    def _decode_to_mel(self, mel_inputs: Tensor) -> Tuple[Tensor, ...]:
        """Creates a single mel spectrogram bin from original mel spectrogram for teacher forcing

        Args:
            mel_inputs: original mel spectrogram

        Returns:
            Tuple of calculated mel spectrogram bin, attention weight and eos
        """
        self._prepare_for_attention(mel_inputs)
        mel_input = self._calculate_attention()
        mel_output = self._calculate_mel_out(mel_input)
        eos = self._calculate_eos()
        return mel_output, self.attn_weights, eos

    def _prepare_for_attention(self, mel_inputs: Tensor):
        """Prepares previous attention states for next iteration with lstm layer

        Args:
            mel_inputs: original mel spectrogram
        """
        cell_input = torch.cat((mel_inputs, self.attn_ctx), -1)
        self.attn_hid, self.attn_cell = self.lstm_cells[0](cell_input, (self.attn_hid, self.attn_cell))
        self.attn_hid = self.attention_dropout(self.attn_hid)

    def _calculate_attention(self) -> Tensor:
        """Calculate attention context and weights

        Returns:
            Tensor of attention context
        """
        self.attn_ctx, self.attn_weights = self.attention(self.attn_hid, self.encoder_state,
                                                          self.attn_processed_encoder_state,
                                                          self.mask_encoder_out)
        return torch.cat((self.attn_hid, self.attn_ctx), -1)

    def _calculate_mel_out(self, mel_input: Tensor) -> Tensor:
        """Calculate output mel spectrogram bin

        Args:
            mel_input: original mel spectrogram

        Returns:
            Tensor of mel spectrogram bin
        """
        self.mel_hid, self.mel_cell = self.lstm_cells[1](mel_input, (self.mel_hid, self.mel_cell))
        self.mel_hid = self.dropout(self.mel_hid)
        self.mel_hid_attn_ctx = torch.cat((self.mel_hid, self.attn_ctx), 1)
        return self.linear_proj(self.mel_hid_attn_ctx)

    def _calculate_eos(self) -> Tensor:
        """Calculates eos sign tensor

        Returns:
            Tensor for eos sign
        """
        return self.eos_proj(self.mel_hid_attn_ctx)

    def _prepare_for_postnet(self, mels: List[Tensor], aligns: List[Tensor], eoss: List[Tensor]) \
            -> Tuple[Tensor, Tensor, Tensor]:
        """Processes data for output and post net network calculation

        Args:
            mels: generated mel spectrogram
            aligns: generated attention weights
            eoss: generated eos sign tensor

        Returns:
            Tuple of mel, attention weights and eos tensors
        """
        aligns = aligns.transpose(0, 1)
        mels = mels.transpose(0, 1).contiguous()
        mels = mels.view(mels.size(0), -1, params.n_mels)
        mels = mels.transpose(1, 2)
        eoss = eoss.transpose(0, 1).contiguous()
        return mels, aligns, eoss

    def _do_prediction_decoding_loop(self, mel_input: Tensor) -> Tuple[Tensor, ...]:
        """Loops to generate mel spectrogram for speech synthesis without teacher forcing and ground truth mel spectrogram

        Args:
            mel_input: tensor to generate bin to

        Returns:
            Tuple of tensors with attention weights, mel spectrogram bins and eos sign tensor
        """
        mels, aligns, eoss = [], [], []
        # inits eos for first check of while loop so that sigmoid of it will not reach threshold
        eos = torch.FloatTensor([[-10.0]])
        while self._did_not_reach_max_iter_or_eos_threshold(eos, mels):
            mel_input = self.prenet(mel_input)
            mel_output, alignments, eos = self._decode_to_mel(mel_input)
            mels.append(mel_output.squeeze(1))
            aligns.append(alignments)
            eoss.append(eos)
            mel_input = mel_output
        return (
            torch.stack(aligns),
            torch.stack(mels),
            torch.stack(eoss)
        )

    def _did_not_reach_max_iter_or_eos_threshold(self, eos: Tensor, mels: List[Tensor]) -> bool:
        """Validates if mel prediction has not reached its threshold.

        Args:
            eos: eos tensor
            mels: mel bins list

        Returns:
            True if model should still generates new bins or False if not
        """
        return (len(mels) <= params.decoder_max_predict_iters
                and torch.sigmoid(eos.data) <= params.decoder_max_eos_val)
