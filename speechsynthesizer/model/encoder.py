"""Module that stores encoder network class for Tacotron model"""
from math import sqrt

import torch
import torch.nn as nn
import torch.nn.utils.rnn as rnn
from torch import Tensor
from torch.nn.utils.rnn import PackedSequence

from .utils import params
from .utils.layers import BatchNormalizedConv1d
from .utils.text import accepted_symbols


class EncoderNetwork(nn.Module):
    """Tacotron encoder module for embedding speech text into a hidden representation for decoder
    """

    def __init__(self):
        """Initializes encoder object with all parameters and neural network layers
        """
        super(EncoderNetwork, self).__init__()
        self.n_layers = params.encoder_n_conv_layers
        self.n_mels = params.n_mels
        self.emb_dim = params.emb_dim
        self.dropout_prob = params.encoder_dropout_prob

        self.embedding: nn.Embedding = self._create_embedding()
        self.convolutions: nn.ModuleList = self._create_convolutions()
        self.lstm: nn.LSTM = self._create_lstm_layer()
        self.dropout = nn.Dropout(self.dropout_prob)

    def forward(self, speech_text: Tensor, text_sizes: Tensor) -> Tensor:
        """Do one iteration of learning of batch of speech text

        Args:
            speech_text: transcription of speech text
            text_sizes

        Returns:
            encoded representation of speech text
        """
        embedded_text: Tensor = self.embedding(speech_text)
        embedded_text = embedded_text.transpose(1, 2)
        for conv in self.convolutions:
            embedded_text = self.dropout(conv(embedded_text))
        embedded_text = embedded_text.transpose(1, 2)
        embedded_text: PackedSequence = self._parse_padded_text(embedded_text, text_sizes)
        return self._calculate_lstm(embedded_text)

    @torch.no_grad()
    def predict(self, speech_text: Tensor) -> Tensor:
        """Generates encoder outputs for audio generation with pretrained model

        Args:
            speech_text: transcription one hot vector tensor

        Returns:
            Tensor with encoded text values
        """
        embedded_text: Tensor = self.embedding(speech_text)
        embedded_text = embedded_text.transpose(1, 2)
        for conv in self.convolutions:
            embedded_text = self.dropout(conv(embedded_text))
        embedded_text = embedded_text.transpose(1, 2)
        return self._calculate_lstm(embedded_text, False)

    @staticmethod
    def _create_embedding() -> nn.Embedding:
        """Creates embedding object

        Returns:
            nn.Embedding object
        """
        return nn.Embedding(len(accepted_symbols), params.emb_dim)

    def _create_convolutions(self) -> nn.ModuleList:
        """Creates a list of convolution layers

        Returns:
            ModuleList of BatchNormalizedConv1D object
        """
        return nn.ModuleList(
            [BatchNormalizedConv1d(self.emb_dim, activation="relu", init_gain="relu")
             for _ in range(params.encoder_n_conv_layers)]
        )

    def _create_lstm_layer(self) -> nn.LSTM:
        """Create lstm layer object

        Returns:
            nn.LSTM layer object
        """
        return nn.LSTM(
            self.emb_dim, int(self.emb_dim / 2), 1,
            bidirectional=True, batch_first=True
        )

    def _parse_padded_text(self, embedded_text: Tensor, text_sizes: Tensor) -> PackedSequence:
        """Packs padded transcription batch

        Args:
            embedded_text: tensor of embedded text transcriptions
            text_sizes: original sizes of transcriptions

        Returns:
            PackedSequence object with packed transcriptions

        Notes:
            https://stackoverflow.com/questions/51030782/why-do-we-pack-the-sequences-in-pytorch
        """
        return rnn.pack_padded_sequence(embedded_text, text_sizes.cpu().numpy(), batch_first=True)

    def _calculate_lstm(self, embedded_text: PackedSequence, training: bool = True) -> Tensor:
        """Calculates LSTM values from embedded transcriptions

        Args:
            embedded_text: embedded transcriptions tensor
            training: is model in training mode bool

        Returns:
            Tensor with calculated encoder outputs for input transcriptions
        """
        if embedded_text.is_cuda:
            self.lstm.flatten_parameters()
        out, _ = self.lstm(embedded_text)
        if training:
            out, _ = rnn.pad_packed_sequence(out, batch_first=True)
        return out
