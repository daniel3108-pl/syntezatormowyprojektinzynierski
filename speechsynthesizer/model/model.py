"""Module that stores entire neural network model class"""
import os.path
from logging import Logger
from typing import Any, Optional, Tuple, Dict

import torch
import torch.nn as nn
import torch.optim as opt

from .decoder import DecoderNetwork
from .encoder import EncoderNetwork
from .utils import func
from ..utils import logger
from ..utils.types import ComputeMode


class MelPredictionNetwork(nn.Module):
    """Main neural network model class,
    that fuses both encoder and decoder with attention together
    """

    LOG: Logger = logger.get_logger("MelPredictionNetwork")

    def __init__(self, encoder: EncoderNetwork, decoder: DecoderNetwork, mode: ComputeMode = None):
        """Initializes model with encoder object and decoder object

        Args:
            encoder: instance of encoder network class
            decoder: instance of decoder network with attention class
            mode: which device to train on, gpu or cpu
        """
        super(MelPredictionNetwork, self).__init__()
        self.encoder = encoder
        self.decoder = decoder
        self.mode = mode

    @classmethod
    def load(cls, file_path: str, mode: ComputeMode = None) -> Tuple['MelPredictionNetwork', Dict[str, Any]]:
        """Loads pretrained model that was saved to the file as a state dict.

        Args:
            file_path: path of the model file
            mode: compute mode either gpu or cpu

        Returns:
            Created and loaded with weights new MelPredictionNetwork object
            and dictionary with optimizer states or None
        """
        if not os.path.exists(file_path):
            raise FileNotFoundError("Could not find file path")
        states: dict[str, Any] = torch.load(file_path, map_location="cpu")
        encoder: EncoderNetwork = cls._load_encoder(states)
        decoder: DecoderNetwork = cls._load_decoder(states)
        model: 'MelPredictionNetwork' = cls._load_model(decoder, encoder, states)
        optimizer: dict[str, Any] = cls._load_optimizer(states)
        cls.LOG.info(f"Model was loaded successfully from file '{file_path}'")
        return model, optimizer

    def forward(self, speech_text: torch.Tensor, mel: torch.Tensor, text_sizes: torch.Tensor) \
            -> Tuple[torch.Tensor, ...]:
        """Method that does network algorithm calculation for training if mel is not None,
        if mel is None it wil predict mel spectrogram

        Args:
            speech_text: transcription one hot vector
            mel: mel spectrogram tensor
            text_sizes: sizes for text input

        Returns:
            tuple of predicted mel spectrogram before decoder's post net,
            predicted mel after decoder's post net and attention alignments (weights)
        """
        encoded_text: torch.Tensor = self.encoder(speech_text, text_sizes.data)
        return self.decoder(encoded_text, mel, text_sizes.data)

    @torch.no_grad()
    def predict(self, speech_text: torch.Tensor) -> torch.Tensor:
        """Does prediction of mel spectrogram based on provided speech transcription

        Args:
            speech_text: encoded transcription of text

        Returns:
            tuple of predicted mel spectrogram before decoder's post net,
            predicted mel after decoder's post net and attention alignments (weights)
        """
        encoded_text: torch.Tensor = self.encoder.predict(speech_text)
        return self.decoder.predict(encoded_text)

    def save(self, filepath: str, optim: Optional[opt.Optimizer] = None):
        """Creates a dictionary with model's weights states and saves it to the file

        Args:
            filepath: path for file to save model in
            optim: optimizer object to save state dict to, it is optional, but highly recommended
        """
        models_states: Dict[str, Any] = self._get_model_states()
        if optim is not None:
            models_states["optim"] = optim.state_dict()
        torch.save(models_states, filepath)
        self.LOG.info(f"Model was save to path: {filepath}")

    @staticmethod
    def _load_encoder(states: Dict[str, Any]) -> EncoderNetwork:
        """Loads Encoder neural network object form it's states

        Args:
            states: model's states

        Returns:
            EncoderNetwork object
        """
        encoder = EncoderNetwork()
        encoder.load_state_dict(states['encoder'])
        return encoder

    @staticmethod
    def _load_decoder(states: Dict[str, Any]) -> DecoderNetwork:
        """Loads Decoder neural network object from it's states

        Args:
            states: model's states

        Returns:
            DecoderNetwork object
        """
        decoder = DecoderNetwork()
        decoder.load_state_dict(states['decoder'])
        return decoder

    @staticmethod
    def _load_model(decoder: DecoderNetwork, encoder: EncoderNetwork, states: Dict[str, Any]) \
            -> 'MelPredictionNetwork':
        """Loads main network's model from model's state dictionary

        Args:
            decoder: Decoder neural network object
            encoder: Encoder neural network object
            states: dictionary of model's state

        Returns:
            MelPredictionNetwork object
        """
        model = MelPredictionNetwork(encoder, decoder)
        model.load_state_dict(states['model'])
        return model

    @staticmethod
    def _load_optimizer(states: Dict[str, Any]) -> Dict[str, Any]:
        """Loads optimizer data from state dict

        Args:
            states: State dictionary

        Returns:
            Dictionary with optimizer's state
        """
        return states.get("optim", None)

    def _get_model_states(self) -> Dict[str, Any]:
        """Gets model's state dict
        """
        return {
            "model": self.state_dict(),
            "encoder": self.encoder.state_dict(),
            "decoder": self.decoder.state_dict(),
        }
