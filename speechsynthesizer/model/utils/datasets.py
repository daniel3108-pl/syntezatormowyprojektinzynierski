"""Module for storing data related classes"""

import os
from logging import Logger
from typing import Tuple, Union, List, Optional

import pandas as pd
import torch
import torchaudio
from torch import Tensor
from torch.utils import data
from torch.utils.data import Subset

from .mel import WaveToMelConverter
from .padding import MelTextBatchCreator
from .text import TextEncoder
from ...utils import ZipFileHandler, logger
from ...utils.config import TrainingConfig


class SpeechSamplesDataset(data.Dataset):
    """Representation object of dataset for model training.
    Stores encoded transcription of speeches and mel spectrograms for wave files
    """

    def __init__(self, csv_file: str, root_dir: str, csv_sep: str, header: Optional[bool],
                 is_zip: bool, zip_handler: ZipFileHandler):
        """Initializes dataset from provided directory path or zip file

        Args:
            csv_file: name of the csv file inside root_dir path
            root_dir: directory or zipfile with dataset
            csv_sep: separator of csv_file
            header: if csv has header
            is_zip: if dataset is in zip file or not
            zip_handler: handler object for zip files
        """
        self.log: Logger = logger.get_logger(self.__class__.__name__)
        self.zip_handler = zip_handler
        self.is_zip = is_zip
        self.root_dir = root_dir
        self.audio_frame: pd.DataFrame = self._get_csv_files(csv_file, csv_sep, root_dir)
        self.log.info(f"Dataset '{root_dir}' loaded successfully")

    def __len__(self) -> int:
        """
        Returns:
            length of dataset
        """
        return self.audio_frame.shape[0]

    def __getitem__(self, idx: Union[int, Tensor]) -> Tuple[Tensor, Tensor]:
        """Creates a single row of transcription and mel spectrogram for speech sample
        Args:
            idx: index of row to get

        Returns:
            tuple of tensors with transcription and mel spectrogram for single sample
        """
        if torch.is_tensor(idx):
            idx = idx.tolist()
        audio_name, sample_rate, waveform = self._get_audio_data(idx)
        mel = self._convert_audio_to_mel(sample_rate, waveform)
        transcription = self._get_transcription(idx)
        return self._create_sample(mel, transcription)

    def train_test_split(self, train_size: float = 0.7, test_size: float = 0.3) -> List[Subset[float]]:
        """Splits dataset into a training dataset and test dataset

        Args:
            train_size: size of training set, defaults with 0.8
            test_size: size of test set, defaults with 0.2

        Returns:
            list of subsets with train and test data
        """
        if train_size + test_size > 1:
            raise ValueError("split sizes cannot be more than 100% in sum")
        train_size = int(len(self) * train_size)
        test_size = len(self) - train_size
        return data.random_split(self, [train_size, test_size])

    def _get_csv_files(self, csv_file: str, csv_sep: str, root_dir: str) -> pd.DataFrame:
        """Reads dataset metadata from csv file

        Args:
            csv_file: metadata csv file path
            csv_sep: csv file sparator
            root_dir: root dir path

        Returns:
            DataFrame object with dataset metadata
        """
        if not self.is_zip:
            return pd.read_csv(root_dir + csv_file, sep=csv_sep, header=None, engine="python")
        return self._get_csv_zipped_files(csv_file, csv_sep, root_dir)

    def _get_zipped_audio_data(self, idx: int) -> Tuple[str, int, Tensor]:
        """Gets audio data from zip file

        Args:
            idx: index of audio file

        Returns:
            Tuple of sample rate int and waveform tensor
        """
        audio_name = os.path.join(self.audio_frame.iloc[idx, 0])
        with self.zip_handler.open(self.root_dir) as zipfile:
            waveform, sample_rate = torchaudio.load(zipfile.get_file(audio_name))
            return audio_name, sample_rate, waveform

    def _get_audio_data(self, idx: int) -> Tuple[str, int, Tensor]:
        """Gets audio data from wave file

        Args:
            idx: index of audio file

        Returns:
            Tuple of sample rate int and waveform tensor
        """
        if not self.is_zip:
            audio_name = os.path.join(self.root_dir, self.audio_frame.iloc[idx, 0])
            waveform, sample_rate = torchaudio.load(audio_name)
        else:
            audio_name, sample_rate, waveform = self._get_zipped_audio_data(idx)
        return audio_name, sample_rate, waveform

    def _get_csv_zipped_files(self, csv_file: str, csv_sep: str, root_dir: str) -> pd.DataFrame:
        """Reads csv metadata from zip dataset file

        Args:
            csv_file: csv file name in zip
            csv_sep: csv file separator
            root_dir: root dir zip file path

        Returns:
            DataFrame object with dataset metadata
        """
        with self.zip_handler.open(root_dir) as zipfile:
            audio_frame = pd.read_csv(zipfile.get_file(csv_file), sep=csv_sep, header=None, engine="python")
            return audio_frame

    @staticmethod
    def _convert_audio_to_mel(sample_rate: int, waveform: Tensor) -> Tensor:
        """Converts audio waveform to mel spectrogram tensor

        Args:
            sample_rate: audio sample rate
            waveform: audio waveform tensor

        Returns:
            Mel spectrogram tensor
        """
        mel_converter = WaveToMelConverter()
        return mel_converter.convert(waveform, sample_rate)

    def _get_transcription(self, idx: int) -> str:
        """Gets transcription from metadata

        Args:
            idx: index of transcription row

        Returns:
            text of speech transcription
        """
        return self.audio_frame.iloc[idx, 1].lower()

    @classmethod
    def _create_sample(cls, mel: Tensor, transcription: str) -> Tuple[Tensor, Tensor]:
        """Creates a single dataset sample with encoded transcription and mel spectrogram tensors

        Args:
            mel: mel spectrogram tensor
            transcription: transcription text

        Returns:

        """
        encoded_transcription: Tensor = cls._get_encoded_transcription(transcription)
        return encoded_transcription, mel

    @classmethod
    def _get_encoded_transcription(cls, transcription: str) -> Tensor:
        """Encodes transcription text into a one hot vector

        Args:
            transcription: transcription text

        Returns:
            Encoded text tensor
        """
        text_encoder = TextEncoder()
        return text_encoder.encode(transcription)


class MelTextDataLoader(data.DataLoader):
    """Dataloader class for mel spectrogram and text data.
    """

    def __init__(self, dataset: data.Dataset, config: TrainingConfig):
        """Initializes object for provided dataset with specified config for batches

        Args:
            dataset: dataset object
            config: configuration object
        """
        super(MelTextDataLoader, self) \
            .__init__(dataset=dataset, batch_size=config.model.batch_size, pin_memory=False,
                      drop_last=True, shuffle=False, collate_fn=MelTextBatchCreator(config.model))
