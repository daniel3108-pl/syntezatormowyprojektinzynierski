"""Module that stores additional utility functions for the model"""
import os
import importlib.machinery
from typing import Tuple, TypeVar, Type

import torch
import torch.nn as nn
from torch import Tensor
from torch.autograd import Variable

from speechsynthesizer.utils.types import ComputeMode


def new_tensor_variable(sizes: Tuple[int, int], source: torch.Tensor) -> Variable:
    """Creates new torch variable of zeros, with provided sizes in 2 element tuple

    Args:
        sizes: tuple with 1 dimension size and 2 dimension size
        source: source tensor to get type and device from

    Returns:
        new variable of zeros with provided dimensions
    """
    target_tensor = torch.zeros(*sizes, device=source.device, dtype=source.dtype)
    return Variable(target_tensor)


def cat_tensor_unsqueeze(t1: Tensor, t2: Tensor, dim: int = 1) -> Tensor:
    """Concatenates two tensors at 1st dimension, but before it unsqueezes both of them at 1st dimension.
    Unsqueezing at 1st dim means transforming tensor from sizes [A, B] to [A, 1, B] shape

    Args:
        t1: first tensor
        t2: second tensor
        dim: dimension on which concatenation is done

    Returns:
        concatenated tensor at 1st dimension
    """
    t1 = t1.unsqueeze(1)
    t2 = t2.unsqueeze(1)
    return torch.cat((t1, t2), dim)


T = TypeVar("T", bound=nn.Module)


def xavier_init_layer(layer_class: Type[T], init_gain: str = "linear", *args, **kwargs) -> T:
    """Creates a new nn.Module object with provided args and keyword args and then initializes its weights using
    Xavier uniform method with provided gain

    Args:
        layer_class: type of layer class to create object
        init_gain: init gain string
        *args: args for layer constructor
        **kwargs: kwargs for layer constructor

    Returns:
        object of layer_class type with initialized weights
    """
    layer = layer_class(*args, **kwargs)
    nn.init.xavier_uniform_(layer.weight, nn.init.calculate_gain(init_gain))
    return layer


def move_to_current_device(mode: ComputeMode, *tensors: torch.Tensor) -> Tuple[Tensor, ...]:
    """Moves tensors to current device

    Args:
        mode: device to load into
        *tensors: list of tensors to move to device

    Returns:
        list of moved tensors to current device
    """
    device: torch.device = get_torch_device(mode)
    tensors_on_device = []
    for i, tensor in enumerate(tensors):
        tensor = tensor.contiguous()
        tensor_on_device = Variable(tensor.to(device, non_blocking=True))
        tensors_on_device.append(tensor_on_device)
    return tuple(tensors_on_device)


def load_params_if_in_cwd():
    """Loads params.py module from cwd if it exists. Used for replacing values without source code.

    Returns:
        new external params module
    """
    if not os.path.exists(os.path.join(".", "params.py")):
        return
    return importlib.machinery.SourceFileLoader("params", "params.py").load_module()


def get_mask(padded_sizes: Tensor, device: torch.device) -> torch.BoolTensor:
    """Creates a mask for padded sizes

    Args:
        padded_sizes: sizes to create mask from
        device: device to send mask tensor to

    Returns:
        Mask created from padded sizes
    """
    max_size = torch.max(padded_sizes).item()
    mask = torch.BoolTensor(len(padded_sizes), max_size)
    for i, size in enumerate(padded_sizes):
        mask[i] = torch.BoolTensor([False if j < size else True
                                    for j in range(max_size)])
    return mask.to(device)


def get_torch_device(mode: ComputeMode) -> torch.device:
    """Maps compute device mode string into a pytorch device object
    Currently supported gpus: Nvidia, Apple Silicon (if you have correct version)

    Args:
        mode: mode literal string, either "cpu" or "gpu"

    Returns:
        device object for gpu if mode is gpu or cpu if mode is cpu
    """
    if mode is None or mode == "cpu":
        return torch.device("cpu")
    return _get_gpu()


def _get_gpu() -> torch.device:
    """Create torch Device object based od on available gpu in the system

    Returns:
        torch.Device object
    """
    if torch.cuda.is_available():
        return torch.device("cuda")
    elif torch.backends.mps.is_available():
        return torch.device("mps")
    else:
        return torch.device("cpu")
