"""Module for storing custom neural network layers"""

from typing import Optional

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import Tensor

from . import params, func
from ...utils.types import ActivationFunctionType, ActivationFunction


class BatchNormalizedConv1d(nn.Module):
    """1 dimensional convolution layer with batch normalization
    """

    def __init__(self,
                 in_channels: int,
                 out_channels: Optional[int] = None,
                 batch_features: Optional[int] = None,
                 kernel: Optional[int] = None,
                 padding: Optional[int] = None,
                 activation: ActivationFunctionType = "relu",
                 init_gain: str = "linear"):
        """Initializes object with convolution layer and batch normalization layer as wel as activation function

        Args:
            in_channels: convolution input channels
            out_channels: convolution output channels
            batch_features: batch normalization features
            kernel: kernel size for convolution
            padding: padding for convolution
            activation: Activation function for output of convolution, values=(relu, tanh)
            init_gain: activation function for weights to be adjusted while inited
        """
        super(BatchNormalizedConv1d, self).__init__()
        self.conv1d: nn.Conv1d = func.xavier_init_layer(
            nn.Conv1d,
            init_gain=init_gain,
            in_channels=in_channels,
            out_channels=out_channels or in_channels,
            kernel_size=kernel or params.conv_kernel_size,
            padding=padding or int((params.conv_kernel_size - 1) / 2),
            stride=1,
            dilation=1,
        )
        self.batch_norm = nn.BatchNorm1d(batch_features or in_channels)
        self.activation: ActivationFunction = self._get_activation_function(activation)

    def forward(self, x: Tensor) -> Tensor:
        """Calculates values using convolution layer and batch normalization layer

        Args:
            x: input tensor for the layer

        Returns:
            Calculated values through the layer
        """
        x = self.activation(self.conv1d(x))
        return self.batch_norm(x)

    @classmethod
    def _get_activation_function(cls, activation: ActivationFunctionType) -> ActivationFunction:
        """Gets a specified activation function object

        Args:
            activation: activation function name

        Returns:
            Activation function object
        """
        if activation == "relu":
            return F.relu
        elif activation == "tanh":
            return torch.tanh
        return lambda x: x
