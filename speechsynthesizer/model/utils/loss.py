"""Module for storing loss functions classes"""
from typing import List, Tuple

import torch
import torch.nn as nn
from torch import Tensor

from . import params
from . import func


class MelPredictionLossFunction(nn.Module):
    """Class for calculation of summed MSE loss of mel prediction.
    It returns a sum of mel prediction losses for both pre post-net and after post-net mel
    """

    def __init__(self):
        """Initializes loss function's losses mean square error layers
        """
        super(MelPredictionLossFunction, self).__init__()
        self.mse_loss_1 = nn.MSELoss()
        self.mse_loss_2 = nn.MSELoss()
        self.bce = nn.BCEWithLogitsLoss()

    def forward(self, output: List[Tensor], target: Tensor, eos_target: Tensor, mel_sizes: Tensor) -> float:
        """Calculates summed mean square error from predicted data based on provided target.

        Args:
            output: model's prediction output in form of a list (mel, mel after postnet, attention alignments, eos)
            target: original mel spectrogram data from the target.
            eos_target: tensor that allows for determining mel's stop point
            mel_sizes: real size of mel spectrograms before padding was applied

        Returns:
            Calculated sum of errors for both mel prediction and mel after post net prediction
        """
        mel_out, post_net_mel_out, eos = self._parse_predictions(output, mel_sizes)
        loss_mel, loss_mel_postnet, loss_eos = self._calculate_losses(mel_out, post_net_mel_out, eos,
                                                                      target, eos_target)
        return loss_mel + loss_mel_postnet + loss_eos

    def _parse_predictions(self, output: Tuple[Tensor, ...], mel_sizes: Tensor) -> Tuple[Tensor, ...]:
        """Parses model's prediction tensors

        Args:
            output: model's predictions
            mel_sizes: original mel spectrograms' sizes

        Returns:
            Tuple of parsed predicted mel spectrograms and eos tensor from the model
        """
        output = self._mask_predicted_values(output, mel_sizes)
        mel_out, post_net_mel_out = output[0], output[1]
        eos = output[3]
        eos = eos.view(-1, 1)
        return mel_out, post_net_mel_out, eos

    def _mask_predicted_values(self, outputs: Tuple[Tensor, ...], sizes: Tensor) -> Tuple[Tensor, ...]:
        """Masks predicted model's mel spectrograms and eos tensors

        Args:
            outputs: model's predictions tensors
            sizes: original mel spectrograms' sizes

        Returns:
            Tuple of masked predicted mel spectrograms and eos tensor from the model
        """
        mask = self._get_mask(sizes, outputs[0].device)
        return self._apply_mask(outputs, mask)

    def _get_mask(self, sizes: Tensor, device: torch.device) -> Tensor:
        """Gets a mask for predictions

        Args:
            sizes: original mel spectrograms' sizes
            device: device to calculate mask on (gpu ot cpu)

        Returns:

        """
        mask = func.get_mask(sizes, device)
        return mask.expand(params.n_mels, mask.size(0), mask.size(1)) \
            .permute(1, 0, 2)

    def _apply_mask(self, output: Tuple[Tensor, ...], mask: Tensor) -> Tuple[Tensor, ...]:
        """Applies mask to predicted mel spectrograms and eos tensors

        Args:
            output: model's mel predictions
            mask: mask to apply

        Returns:
            Tuple of masked prediction from the model
        """
        output[0].data.masked_fill_(mask, 0.0)
        output[1].data.masked_fill_(mask, 0.0)
        output[3].data.masked_fill_(mask[:, 0, :], 1000)
        return output

    def _calculate_losses(self, mel_out: Tensor, post_net_mel_out: Tensor, eos: Tensor,
                          target: Tensor, eos_target: Tensor) -> Tuple[float, float, float]:
        """Calculates losses of the mel prediction

        Args:
            mel_out: predicted mel spectrogram tensor before post net
            post_net_mel_out: predicated mel spectrogram tensor after post net
            eos: predicted eos tensor
            target: target mel spectrogram
            eos_target: target eos tensor

        Returns:

        """
        loss_mel = self.mse_loss_1(mel_out, target)
        loss_mel_postnet = self.mse_loss_2(post_net_mel_out, target)
        loss_eos = self._calculate_eos_loss(eos, eos_target)
        return loss_mel, loss_mel_postnet, loss_eos

    def _calculate_eos_loss(self, eos: Tensor, eos_target: Tensor) -> float:
        """Calculates eos tensor loss

        Args:
            eos: predicted eos tensor
            eos_target: target eos tensor

        Returns:
            eos tensor loss
        """
        eos_target = self._parse_target_eos(eos_target)
        loss_eos = self.bce(eos, eos_target)
        return loss_eos

    def _parse_target_eos(self, eos_target: Tensor) -> Tensor:
        """Parses target eos tensor for bce loss

        Args:
            eos_target: eos target tensor

        Returns:
            parsed eos tensor
        """
        eos_target = eos_target.view(-1, 1)
        eos_target = eos_target.float()
        return eos_target
