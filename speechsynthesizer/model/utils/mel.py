"""Module that stores classes related to converting mel representation
into a wave audio file
"""

import os.path
from logging import Logger

import librosa.effects
import torch
import torchaudio
import torch.nn as nn
import noisereduce as nr
import numpy as np

from torchaudio import transforms
from torch import Tensor

from . import params
from ...utils import logger


class WaveToMelConverter:
    """Converts wave file data into a mel spectrogram tensor
    """

    def convert(self, waveform: Tensor, sample_rate: int) -> Tensor:
        """Converts waveform tensor into a mel tensor

        Args:
            waveform: waveform data
            sample_rate: sample rate

        Returns:
            tensor with converted waveform to mel
        """
        mel = torch.squeeze(self._to_mel_spectrogram(waveform, sample_rate), 0)
        return self._dynamic_range_normalization(mel, norm_const=params.mel_C, clip_value=params.mel_clip_val)

    @staticmethod
    def _to_mel_spectrogram(waveform: Tensor, sample_rate: int) -> Tensor:
        """Converts waveform to mel spectrogram Tensor

        Args:
            waveform: waveform tensor
            sample_rate: sample rate

        Returns:
            Mel spectrogram tensor
        """
        mel_transformer = transforms.MelSpectrogram(
            sample_rate=sample_rate,
            n_fft=params.n_fft,
            win_length=params.win_length,
            hop_length=params.hop_length,
            f_max=params.f_max,
            f_min=params.f_min,
            power=params.power,
            n_mels=params.n_mels,
            mel_scale=params.mel_scale,
            norm=params.mel_norm,
            normalized=params.mel_normalized
        )
        return torch.FloatTensor(mel_transformer(waveform))

    def _dynamic_range_normalization(self, mel: Tensor, norm_const: float, clip_value: float) -> Tensor:
        """Normalizes mel spectrogram by dynamic range

        Args:
            mel: mel spectrogram Tensor
            norm_const: normalization constant
            clip_value: clipping value

        Returns:
            Normalized mel spectrogram tensor
        """
        compressed = torch.clamp(mel, min=clip_value) * norm_const
        return torch.log(compressed)


class MelToWaveConverter:
    """Converts Mel spectrogram to wave file using GriffinLim method
    """

    def __init__(self):
        """Initializes object and creates logger
        """
        self.log: Logger = logger.get_logger(self.__class__.__name__)

    def convert(self, sample_rate: int, mel: Tensor) -> Tensor:
        """Converts mel tensor into a waveform

        Args:
            sample_rate: sample rate of a wave
            mel: mel spectrogram tensor

        Returns:
            tensor with waveform converted from mel
        """
        mel_transformer = self._get_conversion_transformers(sample_rate)
        self.log.info("Started converting mel spectrogram to waveform...")
        mel_denormed = self._dynamic_range_denormalization(mel, norm_const=params.mel_C)
        converted_waveform = mel_transformer(mel_denormed)
        self.log.info("Converted mel spectrogram to waveform")
        return converted_waveform

    def _get_conversion_transformers(self, sample_rate: int) -> torch.nn.Sequential:
        """Creates a Sequential transformer for mel spectrogram to waveform

        Args:
            sample_rate: output sample rate

        Returns:
            torch.nn.Sequential object
        """
        return torch.nn.Sequential(
            self._new_inverse_mel_scale_transformer(sample_rate),
            self._new_griffin_transformer(),
            self._new_noice_reducer(sample_rate)
        )

    def _new_inverse_mel_scale_transformer(self, sample_rate: int) -> transforms.InverseMelScale:
        """Creates a transformer that inverses mel scale

        Args:
            sample_rate: sample rate

        Returns:
            InverseMelScale transformer object
        """
        return transforms.InverseMelScale(
            sample_rate=sample_rate,
            n_mels=params.n_mels,
            n_stft=params.n_stft,
            f_max=params.f_max,
            f_min=params.f_min,
            mel_scale=params.mel_scale,
            norm=params.mel_norm,
            tolerance_loss=params.mel_tolerance_loss,
            tolerance_change=params.mel_tolerance_change,
        )

    def _new_griffin_transformer(self) -> transforms.GriffinLim:
        """Creates a transformer for GriffinLim algorithm to convert inverted mel spectrogram to waveform

        Returns:
            GriffinLim transofrmer object
        """
        return transforms.GriffinLim(
            n_fft=params.n_fft,
            win_length=params.win_length,
            hop_length=params.hop_length,
            power=params.griffim_power,
        )

    def _new_noice_reducer(self, sample_rate: int) -> '_NoiseReducer':
        """Create a _NoiseReducer object for noise reduction of waveform

        Args:
            sample_rate: sample rate

        Returns:
            _NoiseReducer object
        """
        return _NoiseReducer(
            reduction_rate=params.mel_noise_reduction_rate,
            volume_rate=params.mel_noise_volume_increase_rate,
            sr=sample_rate,
            n_ftt=params.n_fft,
            win_length=params.win_length,
            hop_length=params.hop_length,
        )

    def _dynamic_range_denormalization(self, mel: Tensor, norm_const: float) -> Tensor:
        """Reverse dynamic range normalization of mel spectrogram

        Args:
            mel: mel spectrogram Tensor
            norm_const: normalization constant

        Returns:
            Denormalized mel spectrogram Tensor
        """
        return torch.exp(mel) / norm_const


class MelWaveFileWriter:
    """Writer class that creates a wave file from waveform and sample rate
    """

    def __init__(self):
        """Initializes an object and creates a logger
        """
        self.log: Logger = logger.get_logger(self.__class__.__name__)

    def write(self, filepath: str, sample_rate: int, waveform: Tensor):
        """Writes waveform and sample rate to wave file of provided path

        Args:
            filepath: path to save wave to
            sample_rate: sample rate for wave file
            waveform: waveform to save into a file
        """
        if waveform is None:
            raise ValueError("Waveform cannot be None")
        torchaudio.save(filepath + ".wav", waveform, sample_rate, bits_per_sample=16)
        self.log.info(f"Synthesized audio was save to the file: {os.path.abspath(filepath)}.wav")


class _NoiseReducer(nn.Module):
    """Noise reduction module for decreasing static noises after GriffinLim vocoder
    synthesis.
    """

    def __init__(self,
                 reduction_rate: float,
                 volume_rate: float,
                 sr: int,
                 n_ftt: int,
                 win_length: int,
                 hop_length: int,
                 n_jobs: int = -1):
        """Initializes _NoiseReducer object

        Args:
            reduction_rate: rate for noise reduction (between 0.0 and 1.0)
            volume_rate: rate for volume increasing/decreasing
            sr: sample rate of waveform
            n_ftt: size of FFT
            win_length: window size
            hop_length: length of hop between STFT windows
            n_jobs: number of cpu cores to run reduction
        """
        super().__init__()
        self.reduction_rate = reduction_rate
        self.volume_rate = volume_rate
        self.sr = sr
        self.n_ftt = n_ftt
        self.win_length = win_length
        self.hop_length = hop_length
        self.n_jobs = n_jobs

    def forward(self, waveform: Tensor) -> Tensor:
        """Reduces noises from waveform by reduction_rate and changes its volume by volume_rate.
        It also trims silence.

        Args:
            waveform: waveform to reduce noise from

        Returns:
            Noise reduced and with changed volume waveform for output wave file
        """
        waveform_np = waveform.cpu().numpy()
        reduced = nr.reduce_noise(waveform_np,
                                  sr=self.sr,
                                  prop_decrease=self.reduction_rate,
                                  n_fft=self.n_ftt,
                                  win_length=self.win_length,
                                  hop_length=self.hop_length,
                                  n_jobs=self.n_jobs)
        reduced = self._change_volume(reduced)
        reduced = librosa.effects.trim(reduced)
        return torch.as_tensor(reduced).float()

    def _change_volume(self, waveform: np.array) -> np.array:
        """Change volume of output audio

        Args:
            waveform: waveform array

        Returns:
            array of output waveform
        """
        return waveform * (self.volume_rate) + 0.5
