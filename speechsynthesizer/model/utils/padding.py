"""Module that stores data batch creator class"""

from typing import Any, List, Tuple

import torch
from torch import Tensor, LongTensor

from .text.consts import pad
from ...utils.config import ModelConfig


class MelTextBatchCreator:
    """Class that creates batches for torch.utils.data.DataLoader collate_fn argument
    """

    def __init__(self, config: ModelConfig):
        """Initializes object with model config

        Args:
            config: model training configuration object
        """
        self.config = config

    def __call__(self, data: List[Any]) -> Tuple[Tensor, ...]:
        """Creates a single batch of data

        Args:
            data: list of tensors with single batch data before padding

        Returns:
            process and padded batch data
        """
        text_vectors = [row[0] for row in data]
        mel_vectors = [row[1] for row in data]
        return self._pad_vectors(text_vectors, mel_vectors)

    def _pad_vectors(self, text_vectors: List[Tensor], mel_vectors: List[Tensor]) -> Tuple[Tensor, ...]:
        """Pads single batch tensors

        Args:
            text_vectors: list of all transcriptions in batch
            mel_vectors: list of all mel spectrograms in batch

        Returns:
            Tuple of padded tensors with mel spectrogram, speech texts, sizes of mel spectrograms and transcription
            and end of sentence sign tensor
        """
        longest_seq, text_lens, sorted_idxs = self._get_max_len_of_text_vectors(text_vectors)
        size_of_biggest_mel_vec = self._get_size_of_biggest_mel_vec(mel_vectors)
        padded_text_vectors = []
        mel_vectors_arr, eos_vectors_arr = [], []
        mel_sizes = torch.LongTensor(len(mel_vectors))
        for i, idx in enumerate(sorted_idxs):
            padded_vec: List[int] = self._pad_single_text_vec(text_vectors[idx], longest_seq)
            padded_text_vectors.append(padded_vec)
            mel = self._pad_single_mel_vec(mel_vectors[idx], size_of_biggest_mel_vec)
            mel_vectors_arr.append(mel)
            eos_vectors_arr.append(self._create_padded_eos(mel_vectors[idx], size_of_biggest_mel_vec))
            mel_sizes[i] = mel_vectors[idx.item()].size(1)
        return (torch.tensor(padded_text_vectors).long(),
                torch.tensor(mel_vectors_arr).float(),
                torch.tensor(eos_vectors_arr).float(),
                text_lens, mel_sizes)

    def _get_max_len_of_text_vectors(self, text_vectors: List[Tensor]) -> Tuple[int, Tensor, List[int]]:
        """Gets max length of transcription in transcriptions list

        Args:
            text_vectors: list of transcriptions

        Returns:
            Tuple of max length, sorted lengths of transcriptions and indices of sorted lengths in original tensor
        """
        vec_len_tensor = LongTensor([len(vec) for vec in text_vectors])
        vec_len_tensor, sorted_ids_text = torch.sort(vec_len_tensor, dim=0, descending=True)
        return int(torch.max(vec_len_tensor)), vec_len_tensor, sorted_ids_text

    def _pad_single_text_vec(self, vec: Tensor, size: int) -> List[int]:
        """Pads single transcription vector

        Args:
            vec: transcription Tensor
            size: size of longest transcription to pad with

        Returns:
            Padded transcription list of values
        """
        vec_size = len(vec)
        left_to_pad = size - vec_size
        vec = vec.tolist()
        vec.extend([pad for _ in range(left_to_pad)])
        return vec

    def _get_size_of_biggest_mel_vec(self, mel_vectors: List[Tensor]) -> torch.Size:
        """Gets a size of the biggest mel spectrogram tensor

        Args:
            mel_vectors: list of mel spectrograms in batch

        Returns:
            torch.Size object with biggest mel size
        """
        dim2_mel_sizes_vec = LongTensor([mel.size(1) for mel in mel_vectors])
        dim2_mel_sizes_vec, _ = torch.sort(dim2_mel_sizes_vec, dim=0, descending=True)
        max_2dim = int(torch.max(dim2_mel_sizes_vec))
        return torch.Size([mel_vectors[0].size(0), max_2dim])

    def _pad_single_mel_vec(self, mel_vec: Tensor, size_of_biggest_mel_vec: torch.Size) -> List[List[Any]]:
        """Pads a single mel spectrogram tensor

        Args:
            mel_vec: mel spectrogram tensor
            size_of_biggest_mel_vec: size of biggest mel to padd with

        Returns:
            Padded mel spectrogram
        """
        mel_vec = mel_vec.tolist()
        padded_mel_vec = []
        for mel_bin in mel_vec:
            bin_list = list(mel_bin)
            bin_list.extend([pad for _ in range(size_of_biggest_mel_vec[1] - len(bin_list))])
            padded_mel_vec.append(bin_list)
        return padded_mel_vec

    def _create_padded_eos(self, mel_vec: Tensor, biggest_size: torch.Size) -> List[int]:
        """Creates a padded end of sentence sign vector

        Args:
            mel_vec: mel spectrogram tensor
            biggest_size: size of the biggest mel spectrogram in the batch

        Returns:
            Padded end of sentence sign list
        """
        size = biggest_size[1]
        eos = [0 for _ in range(mel_vec.size(1) - 1)]
        eos.extend([1 for _ in range(mel_vec.size(1) - 1, size)])
        return eos
