"""Module with constants for model creation and training"""

# Common consts
emb_dim = 512
conv_kernel_size = 5
dropout_prob = 0.5
weight_decay = 1e-6
epsilon = 1e-6
max_grad_clip_norm = 1.0

# Dataset consts
train_size = 0.90
test_size = 0.10
csv_separator = "    "
csv_has_header = None  # None means no header (same value you would add to pandas read_csv)

# if exponential lr decay is set to None,
# decay will be disabled and learning rate won't change after loss exceed threshold
exponential_lr_decay = None   # 0.1
exponential_lr_decay_loss_thresh = 16000  # In iters
exponential_lr_step_iter = 10000
exponential_lr_decay_min_value = 1e-5

# Encoder consts
encoder_n_conv_layers = 3
encoder_n_lstm_layers = 1
encoder_dropout_prob = 0.5

# Decoder consts
decoder_prenet_dim = 256
decoder_n_lstm_units = 1024
decoder_postnet_n_conv_layers = 5
decoder_max_predict_iters = 1000
decoder_max_eos_val = 0.5
decoder_eos_dim = 1536
decoder_dropout = 0.1

# Attention consts
attention_n_filters = 32
attention_loc_kernel_size = 31
attention_rnn_dim = 1024
attention_hid_dim = 128
attention_dropout = 0.1

# Mel consts
n_mels = 80
n_fft = 1024
n_stft = n_fft // 2 + 1
win_length = 1024
hop_length = 256
f_max = 7600
f_min = 95
power = 2
griffim_power = 1.5
mel_norm = "slaney"
mel_scale = "slaney"
mel_tolerance_loss = 1e-5
mel_tolerance_change = 1e-8
mel_normalized = False
mel_C = 1
mel_clip_val = 1e-5
mel_noise_reduction_rate = 0.99
mel_noise_volume_increase_rate = 3.0


# Loads dynamically external params from params.py class in current working directory
from .func import load_params_if_in_cwd
_params_ext = load_params_if_in_cwd()
if _params_ext is not None:
    for name, val in _params_ext.__dict__.items():
        if not name.startswith("__"):
            vars()[name] = val
    print("Loaded additional external params from params.py from cwd.")
