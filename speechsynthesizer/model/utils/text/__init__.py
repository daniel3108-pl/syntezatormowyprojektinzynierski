"""Package for text related functions, constants and classes"""

from .consts import *
from .manipulators import *
