"""module with text constants"""

import string
from typing import List


def _create_accepted_symbols() -> List[str]:
    """Creates list of symbols that are acceptable in the network

    Returns:
        list of characters
    """
    _padding = "_"
    _signs = "-!'(),.:;? "
    _letters = string.ascii_lowercase
    return list(_padding) + list(_signs) + list(_letters)


accepted_symbols = _create_accepted_symbols()
"""Symbols that are accepted by the program, used to clean user input"""

pad = 0
"""index for padding in accepted symbol"""

abbreviation_map = {
    "mr.": "mister",
    "mrs.": "misess",
    "dr.": "doctor",
    "no.": "number",
    "st.": "saint",
    "co.": "company",
    "jr.": "junior",
    "maj.": "major",
    "gen.": "general",
    "drs.": "doctors",
    "rev.": "reverend",
    "lt.": "lieutenant",
    "hon.": "honorable",
    "sgt.": "sergeant",
    "capt.": "captain",
    "esq.": "esquire",
    "ltd.": "limited",
    "col.": "colonel",
    "ft.": "foot",
    "lbs.": "pounds",
    "lb.": "pound",
}
"""Map with english language abbreviation and its full meaning"""
