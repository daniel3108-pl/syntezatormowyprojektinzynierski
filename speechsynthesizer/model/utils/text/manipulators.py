"""Module with classes relate to manipulating of text data and its encoded format"""

import re
from typing import Optional, List, Dict

import torch

from . import consts


class TextEncoder:
    """Encodes text to continuous one hot vector of character index in character to idx map
    """

    def encode(self, text: str) -> Optional[torch.LongTensor]:
        """Encoded provided text

        Args:
            text: text to encode

        Returns:
            tensor with encoded values
        """
        if not text or text.strip() == "":
            return None
        cleaned_text: str = self._clean_text(text)
        encoded_text: List[int] = self._sequence_text(cleaned_text)
        return torch.LongTensor(encoded_text)


    def _sequence_text(self, text: str) -> List[int]:
        """Creates a sequence of text with indexes in accepted symbols list

        Args:
            text: text to sequence

        Returns:
            Sequenced text with indexes List
        """
        char_to_idx_dict: Dict[str, int] = self._get_char_to_idx_dict()
        sequenced_text = [char_to_idx_dict[char] for char in text]
        return sequenced_text

    @staticmethod
    def _get_char_to_idx_dict() -> Dict[str, int]:
        """Gets dictionary of character to its index

        Returns:
            Dictionary object
        """
        symbols = enumerate(consts.accepted_symbols)
        return {char: idx for idx, char in symbols}

    @staticmethod
    def _clean_text(text: str) -> str:
        """Cleans provided text

        Args:
            text: text to clean

        Returns:
            Cleaned text
        """
        cleaner = TextCleaner()
        return cleaner.clean(text)


class TextDecoder:
    """Converts one hot vector into a text using index to character map that reflects char to index map
    from TextEncoder
    """

    def decode(self, encoded_text: List[int]) -> str:
        """Decodes sequence to standard characters

        Args:
            encoded_text: list of encoded values for text

        Returns:
            string with decoded text
        """
        if not encoded_text or len(encoded_text) == 0:
            return ""
        decoded_text = ""
        for idx in encoded_text:
            decoded_text += self._get_char_from_idx(idx)
        return decoded_text

    @classmethod
    def _get_char_from_idx(cls, idx: int) -> str:
        """Gets a character with specified index from index to character dictionary

        Args:
            idx: index of character

        Returns:
            Character from dictionary
        """
        idx_to_char_dict = cls._get_idx_to_char_dict()
        return idx_to_char_dict.get(idx, "")  # Gets a char or empty string if not present in dict

    @classmethod
    def _get_idx_to_char_dict(cls) -> Dict[int, str]:
        """Gets a dictionary of index to character

        Returns:
            Dictionary object
        """
        symbols = enumerate(consts.accepted_symbols)
        return {idx: char for idx, char in symbols}


class TextCleaner:
    """Cleans provided text to meet encoder and model expectation
    """

    def clean(self, text: str) -> str:
        """Cleans text

        Args:
            text: text to clean

        Returns:
            string with cleaned text
        """
        if not text or text == "":
            return ""
        text: str = text.lower()
        text: str = self._delete_repeated_whitespaces(text)
        text: str = self._remove_non_acceptable_symbols(text)
        text: str = self._map_abbreviations(text)
        return text

    @staticmethod
    def _delete_repeated_whitespaces(text: str) -> str:
        """Deletes repeated whitespaces from text

        Args:
            text:

        Returns:
            text without repeated whitespaces
        """
        return re.sub(" +", " ", text)

    @classmethod
    def _remove_non_acceptable_symbols(cls, text: str) -> str:
        """Removes non acceptable symbols from text

        Args:
            text:

        Returns:
            text without illegal symbols
        """
        cleaned_symbols_list = [char for char in text 
                                if cls._is_symbol_acceptable(char)]
        return "".join(cleaned_symbols_list)

    @classmethod
    def _is_symbol_acceptable(cls, char: str) -> bool:
        """Checks if symbols is in accepted symbols list

        Args:
            char: character to validate

        Returns:
            True if character is acceptable or False if not
        """
        return char in consts.accepted_symbols

    @staticmethod
    def _map_abbreviations(text: str) -> str:
        """Map abbreviations to its full text from abbreviation_map

        Args:
            text:

        Returns:
            Text with all abbreviations resolved
        """
        for abbrv in consts.abbreviation_map.keys():
            if abbrv in text:
                text = text.replace(abbrv, consts.abbreviation_map[abbrv])
        return text
