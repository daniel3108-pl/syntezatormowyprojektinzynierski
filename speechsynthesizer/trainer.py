"""Module with class for model creation"""
import os
import random
import time
from logging import Logger
from typing import Optional, Any, Tuple, Dict, List

import numpy as np
import torch
import torch.optim as optim
import torch.nn as nn
from torch import Tensor
from torch.optim.lr_scheduler import ExponentialLR
import matplotlib.pyplot as plt

from .model import DecoderNetwork
from .model import EncoderNetwork
from .model.model import MelPredictionNetwork
from .model.utils import SpeechSamplesDataset, MelTextDataLoader, func, params
from .model.utils.loss import MelPredictionLossFunction
from .utils import ReportPrinter
from .utils import ZipFileHandler
from .utils import logger
from .utils.config import TrainingConfig, ConfigLoader, DatasetConfigValidator
from .utils.types import ComputeMode
from .utils import messages


class TextToSpeechTrainer:
    """Class for training speech synthesis model and then exporting it to a file
    """

    def __init__(self, config_path: Optional[str], report: Optional[str], mode: ComputeMode):
        """Parses config, dataset and data loaders, creates network object
        and all classification params

        Args:
            config_path: path to config file to load
            report: path to report file to generate
            mode: mode in which application will train the model
        """
        self.log: Logger = logger.get_logger(self.__class__.__name__)
        self.config_path = config_path
        self.config: TrainingConfig = self._load_config()
        self.report = report
        self.mode = mode
        self.training_iter = 0
        self._set_seed_if_in_config()

        self.dataset: SpeechSamplesDataset = self._load_training_data()
        self.train_set, self.test_set = self.dataset.train_test_split(train_size=params.train_size, test_size=params.test_size)
        self.train_loader = MelTextDataLoader(self.train_set, self.config)
        self.test_loader = MelTextDataLoader(self.test_set, self.config)
        self.train_set_size = len(self.train_loader)
        self.test_set_size = len(self.test_loader)
        self.no_batches = self.train_set_size // self.config.model.batch_size

        self.model, self.optimizer = self._init_model()
        if params.exponential_lr_decay is not None:
            self.learning_rate_decay = ExponentialLR(self.optimizer, params.exponential_lr_decay)
        self.criterion = MelPredictionLossFunction()
        self.log.info("Model trainer module was initialized with provided settings")

    def train(self):
        """Runs training process, saves model to a file and runs report generation
        if user provided report file path
        """
        self.log.info(messages.started_learning(self.config.model.epochs, self.config.model.batch_size))
        try:
            results: Dict[str, List[float]] = self._do_training_loop(self.config.model.epochs)
            self._save_model()
            self._print_report_if_chosen(results)
        except KeyboardInterrupt:
            self.log.warning(messages.learning_interrupted())
            self._save_model()

    def _save_model(self):
        """Saves neural network model to file
        """
        self.model.save(self.config.model.model_file, self.optimizer)

    def _load_config(self) -> TrainingConfig:
        """Loads training config from the user

        Returns:
            Training Config object
        """
        if self.config_path is None:
            self.log.warning("No config file provided. Loading default settings.")
            return TrainingConfig.default()
        zip_handler = ZipFileHandler()
        validator = DatasetConfigValidator(zip_handler)
        loader = ConfigLoader(validator)
        return loader.load(self.config_path)

    def _set_seed_if_in_config(self):
        """Sets random seed for all used randomizers
        """
        random_seed: Optional[int] = self.config.model.random_seed
        if random_seed is not None and isinstance(random_seed, int):
            torch.random.manual_seed(random_seed)
            torch.cuda.random.manual_seed(random_seed)
            np.random.seed(random_seed)
            random.seed(random_seed)

    def _load_training_data(self) -> SpeechSamplesDataset:
        """Loads training data

        Returns:
            SpeechSamplesDataset object that represents training data
        """
        root_dir: str = self.config.dataset.root_dir
        definition_f: str = self.config.dataset.definition_file
        train_data = SpeechSamplesDataset(definition_f, root_dir,
                                          csv_sep=params.csv_separator,
                                          header=params.csv_has_header,
                                          is_zip=root_dir.endswith('.zip'),
                                          zip_handler=ZipFileHandler())
        return train_data

    def _init_model(self) -> Tuple[MelPredictionNetwork, optim.Adam]:
        """Initializes neural network model object

        Returns:
            Tuple of MelPredictionNetwork object and Adam optimizer object
        """
        if os.path.exists(self.config.model.model_file):
            return self._load_checkpoint()
        encoder = EncoderNetwork()
        decoder = DecoderNetwork()
        device = func.get_torch_device(self.mode)
        if device == torch.device("cuda"):
            self.log.info("Graphics card was detected. Using it to train the model.")
            model = MelPredictionNetwork(encoder.to(device), decoder.to(device), self.mode).to(device)
        else:
            model = MelPredictionNetwork(encoder, decoder, self.mode)
        return model, self._new_optimizer(model)

    def _load_checkpoint(self) -> Tuple[MelPredictionNetwork, optim.Adam]:
        """Loads pretrained network model from file provided in the config.

        Returns:
            Tuple of MelPrediction object and Adam optimizer object
        """
        self.log.info("Already existing model file have been detected. Proceeding to load it for further training.")
        model, opt_states = MelPredictionNetwork.load(self.config.model.model_file, self.mode)
        model.train()
        device = func.get_torch_device(self.mode)
        if device == torch.device("cuda"):
            self.log.info("Graphics card was detected. Using it to train the model.")
            model = model.to(device)
        optimizer = self._new_optimizer(model)
        if opt_states is not None:
            optimizer.load_state_dict(opt_states)
        return model, optimizer

    def _new_optimizer(self, model: MelPredictionNetwork) -> optim.Adam:
        """Creates new Adam optimizer object
        Args:
            model: Neural network model object
        """
        return optim.Adam(model.parameters(),
                          lr=self.config.model.learning_rate,
                          eps=params.epsilon,
                          weight_decay=params.weight_decay)

    def _do_training_loop(self, epochs: int) -> Dict[str, List[float]]:
        """Runs training loop

        Args:
            epochs: current epoch index

        Returns:
            Dictionary with training and validation loss
        """
        results = {'tloss': [], 'vloss': []}  # tloss = Training loss, vloss = Validation/Test loss
        for i in range(0, epochs):
            start_time = time.perf_counter()
            self._train_one_epoch(results, i)
            self._evaluate_model_one_epoch(results, i)
            end_time = time.perf_counter() - start_time
            self._print_epoch_log(results, i, end_time)
            if (i + 1) % self.config.model.epochs_per_checkpoint == 0:
                self.log.info(f"Saving model checkpoint after {self.config.model.epochs_per_checkpoint} epochs "
                              f"in '{self.config.model.model_file}'...")
                self._save_model()
        return results

    def _train_one_epoch(self, results: Dict[str, Any], i: int):
        """Runs single training iteration

        Args:
            results: training and validation losses dictionary
            i: current epoch index
        """
        ep_loss = 0
        self.model.train()
        for j, batch in enumerate(self.train_loader):
            transcription, mel, eos, text_sizes, mel_sizes = func.move_to_current_device(self.mode, *batch)
            self.model.zero_grad()
            ep_loss = self._train_batch(ep_loss, eos, i, j, mel, text_sizes, transcription, mel_sizes)
            self.training_iter += 1
        results['tloss'].append(ep_loss / self.train_set_size)

    def _train_batch(
            self, ep_loss: float,
            eos: Tensor,
            i: int, j: int,
            mel: Tensor,
            text_sizes: Tensor,
            transcription: Tensor,
            mel_sizes: Tensor
    ) -> float:
        """Runs training iteration for single batch

        Args:
            ep_loss: loss from current epoch
            eos: ground truth end of sentence sign tensor
            i: current epoch index
            j: current batch iteration index
            mel: ground truth mel spectrogram batch tensor
            text_sizes: Tensor of original sizes of transcriptions from the batch
            transcription: Tensor of padded transcriptions from the batch
            mel_sizes: Original sizes of the padded mel spectrograms

        Returns:
            sum of entire epoch loss with current batch loss
        """
        prediction = self.model(transcription, mel, text_sizes)
        loss = self._calculate_loss(prediction, mel, eos, mel_sizes)
        self.log.info(f"Batch ({j + 1}/{self.train_set_size}) "
                      f"of epoch {i + 1} trained with loss {loss}")
        return ep_loss + loss

    def _calculate_loss(self, prediction: Tensor, mel: Tensor, eos: Tensor, mel_sizes: Tensor) -> float:
        """Calculates loss of model's prediction

        Args:
            prediction: Tensor of predicted mel spectrograms and eos tensor
            mel: ground truth mel spectrograms
            eos: ground truth eos tensors
            mel_sizes: original sizes of ground truth spectrograms

        Returns:
            loss of current batch iteration
        """
        loss = self.criterion(prediction, mel, eos, mel_sizes.data)
        loss_item = loss.item()
        loss.backward()
        nn.utils.clip_grad_norm_(self.model.parameters(), max_norm=params.max_grad_clip_norm)
        self.optimizer.step()
        self._decay_lr_if_should()
        return loss_item

    def _decay_lr_if_should(self):
        """Decays learning rate if it meets provided requirements for that
        """
        if (params.exponential_lr_decay is not None
                and self.training_iter >= params.exponential_lr_decay_loss_thresh
                and (self.training_iter - params.exponential_lr_decay_loss_thresh) % params.exponential_lr_step_iter == 0
                and self.optimizer.param_groups[0]["lr"] > params.exponential_lr_decay_min_value):
            self.learning_rate_decay.step()

    def _evaluate_model_one_epoch(self, results: Dict[str, Any], i: int):
        """Runs a single validation epoch iteration

        Args:
            results: training and validation loss dictionary
            i: current epoch index
        """
        self.model.eval()
        ep_loss = 0
        with torch.no_grad():
            for j, batch in enumerate(self.test_loader):
                transcription, mel, eos, text_sizes, mel_sizes = func.move_to_current_device(self.mode, *batch)
                ep_loss = self._eval_batch(ep_loss, eos, i, j, mel, text_sizes, transcription, mel_sizes)
        results['vloss'].append(ep_loss / self.test_set_size)

    def _eval_batch(
            self, ep_loss: float,
            eos: Tensor,
            i: int, j: int,
            mel: Tensor,
            text_sizes: Tensor,
            transcription: Tensor,
            mel_sizes: Tensor,
    ) -> float:
        """Evaluates single batch for validation

        Args:
            ep_loss: loss of entire validation epoch
            eos: ground truth eos tensor
            i: epoch index
            j: batch index
            mel: ground truth mel spectrogram
            text_sizes: original transcription sizes
            transcription: ground truth transcriptions
            mel_sizes: original mel spectrogram sizes

        Returns:
            sum of entire validation epoch loss and current batch loss
        """
        prediction = self.model(transcription, mel, text_sizes)
        loss = self._calculate_loss_validation(prediction, mel, eos, mel_sizes)
        ep_loss += loss
        self.log.info(f"Batch ({j + 1}/{self.test_set_size}) "
                      f"of epoch {i + 1} validated with loss {loss}")
        return ep_loss

    def _calculate_loss_validation(self, prediction: Tensor, mel: Tensor, eos: Tensor, mel_sizes: Tensor) -> float:
        """Calculates validation error for single batch

        Args:
            prediction: model's predictions
            mel: ground truth mel spectrograms
            eos: ground truth eos tensors
            mel_sizes: original mel sizes

        Returns:
            validation error
        """
        loss = self.criterion(prediction, mel, eos, mel_sizes.data)
        return loss.item()

    def _print_epoch_log(self, results: Dict[str, Any], i: int, time: float):
        self.log.info(f"EPOCH [{i + 1}/{self.config.model.epochs}] "
                      f"| Train loss: {results.get('tloss')[i]} "
                      f"| Test loss: {results.get('vloss')[i]} "
                      f"| Time: {time}s")

    def _print_report_if_chosen(self, results: Dict[str, Any]):
        """Prints pdf report from training process if user has provided path for it

        Args:
            results: training and validation losses dictionary
        """
        if self.report is None:
            return
        report_printer: ReportPrinter = self._create_report_printer(results)
        report_printer.save_report(self.report)

    def _create_report_printer(self, results: Dict[str, Any]) -> ReportPrinter:
        """Creates ReportPrinter object

        Args:
            results: training and validation losses dict

        Returns:
            ReportPrinter object
        """
        return ReportPrinter(
            config_path=self.config_path,
            config=self.config,
            dataset=self.config.dataset.root_dir,
            **results
        )


def _plot_data(mel_spectrogram: torch.Tensor, mel_alignments: torch.Tensor):
    """For model debugging process
    """
    fig, axes = plt.subplots(1, 2, figsize=(16, 4))
    axes[0].set_title("Mel spectrogram")
    axes[0].imshow(mel_spectrogram.float().data[0], aspect='auto', origin='lower',
                   interpolation='none')
    axes[1].set_title("Mel alignments")
    axes[1].imshow(mel_alignments.float().data[0].T, aspect='auto', origin='lower',
                   interpolation='none')
    plt.show()