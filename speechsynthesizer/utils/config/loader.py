"""Module that stores classes that are related to loading user defined
yaml configuration file"""

import os
import sys
from logging import Logger
from typing import Any, Optional, List, Dict

import pydantic
import yaml

from .objs import DatasetConfig, TrainingConfig
from .. import logger
from ..exceptions import ConfigLoadingUnsuccessfulError
from ..zip import ZipFileHandler


class DatasetConfigValidator:
    """Takes care of validation of DatasetConfig class
    """

    def __init__(self, zip_handler: ZipFileHandler):
        """Initialize object and creates logger object

        Args:
            zip_handler: object of ZipFileHandler for validation of dataset in zip format
        """
        self.log: Logger = logger.get_logger(self.__class__.__name__)
        self.zip_handler = zip_handler

    def validate(self, dataset_config: DatasetConfig) -> bool:
        """Validates if dataset config presents correct and existing values/data

        Args:
            dataset_config: config object to validate

        Returns:
            True if config is valid or False if it's not
        """
        if dataset_config is None or not isinstance(dataset_config, DatasetConfig):
            return False
        return self._has_proper_dataset_values(dataset_config)

    def _has_proper_dataset_values(self, config: DatasetConfig) -> bool:
        if not self._is_root_dir_correct(config):
            self.log.warning("Dataset does not exists.")
            return False
        if self._is_root_dir_zip(config):
            self.log.info("Dataset in zip archive was detected. Started handling zip file.")
            return self._handle_root_dir_being_zip(
                config.root_dir,
                config.definition_file,
                config.audio_directory
            )
        return self._do_dataset_files_exists(config)

    @staticmethod
    def _is_root_dir_correct(config: DatasetConfig) -> bool:
        """Checks if root dir of DatasetConfig is correct

        Args:
            config: DatasetConfig object

        Returns:
            True if is correct False if is not
        """
        return os.path.exists(config.root_dir)

    @staticmethod
    def _is_root_dir_zip(config: DatasetConfig) -> bool:
        """Checks if root dir is a zip file

        Args:
            config: DatasetConfig object

        Returns:
            True if is zip file False if is not
        """
        return config.root_dir.endswith(".zip") \
               and os.path.isfile(config.root_dir)

    def _handle_root_dir_being_zip(self, root_dir: str, def_file: str, wav_dir: str) -> bool:
        """Validates root directory when it is a zip file

        Args:
            root_dir: path to root dir
            def_file: definition file path
            wav_dir: audio directory path

        Returns:
            True if zip structure is correct of False if is not
        """
        with self.zip_handler.open(root_dir) as zipfile:
            filenames = zipfile.filenames()
            return self._are_zip_files_correct(def_file, filenames, wav_dir)

    def _are_zip_files_correct(self, def_file: str, filenames: List[str], wav_dir: str) -> bool:
        """Checks if zip files are correct

        Args:
            def_file: path to definition file
            filenames: filenames to check in zip
            wav_dir: path to directory of audio files

        Returns:
            True if is correct False if not
        """
        if def_file not in filenames:
            self.log.error(f"Metadata file {def_file} was not found")
            return False
        if wav_dir not in filenames:
            self.log.error(f"No wav files dir {wav_dir}")
            return False
        return True

    def _do_dataset_files_exists(self, config: DatasetConfig) -> bool:
        """Checks if files specified in DatasetConfig object exist

        Args:
            config: DatasetConfig object

        Returns:
            True if they exist False if not
        """
        audio_dir: str = self._get_audio_dir(config)
        if not os.path.exists(audio_dir) or len(os.listdir(audio_dir)) < 1:
            self.log.warning(f"Audio directory does not exists or is empty")
            return False
        if not os.path.exists(self._get_definition_file(config)):
            return False
        return True

    @staticmethod
    def _get_audio_dir(config: DatasetConfig) -> str:
        """Gets absolute audio directory

        Args:
            config: DatasetConfig object

        Returns:
            String object with audio directory
        """
        return config.root_dir + config.audio_directory

    @staticmethod
    def _get_definition_file(config: DatasetConfig) -> str:
        """Gets absolute definition file directory

        Args:
            config: DatasetConfig object

        Returns:
            String object with definition file directory
        """
        return config.root_dir + config.definition_file


class ConfigLoader:
    """Takes care of loading configuration for training from yaml file and validates that config
    """

    def __init__(self, validator: DatasetConfigValidator):
        """Initialize object and creates logger.

        Args:
            validator: validator object for dataset config
        """
        self.log: Logger = logger.get_logger(self.__class__.__name__)
        self.validator = validator
        self.file = ""

    def load(self, file: str) -> TrainingConfig:
        """Loads config file into a TrainingConfig object

        Args:
            file: path to config file in yaml format

        Returns:
            Loaded config object if not error occur or default training configuration,
             using TrainingConfig.default() method
        """
        self.file = file
        self.log.info(f"Starting loading config file of path: '{self.file}'")
        if not os.path.exists(self.file):
            raise ConfigLoadingUnsuccessfulError(f"Provided config file '{self.file}' does not exists. ")
        return self._load()

    def _load(self) -> TrainingConfig:
        """Loads config file into a TrainingConfig object

        Returns:
            TrainingConfig object with loaded parameters
        """
        config: Optional[TrainingConfig] = self._load_config_from_file()
        if self._config_is_valid(config):
            self.log.info(f"Provided training config '{self.file}' loaded successfully")
            return config
        raise ConfigLoadingUnsuccessfulError("Incorrect config values.")

    def _load_config_from_file(self) -> Optional[TrainingConfig]:
        """Loads config from file

        Returns:
            TrainingConfig object if loaded successfully or None if not
        """
        try:
            return TrainingConfig.from_dicts(**self._get_config_dict())
        except (TypeError, pydantic.ValidationError):
            self.log.error(str(sys.exc_info()[1]))
            return None

    def _get_config_dict(self) -> Dict[str, Any]:
        """Gets dictionary of config parameters

        Returns:
            Dictionary object with config
        """
        with open(self.file, "r") as f:
            config_dict = yaml.load(f, yaml.Loader)
        return config_dict

    def _config_is_valid(self, config: TrainingConfig) -> bool:
        """Check if config is valid

        Args:
            config: TrainingConfig object to validate

        Returns:
            True if config is valid or False if not
        """
        return config is not None \
               and self.validator.validate(config.dataset)
