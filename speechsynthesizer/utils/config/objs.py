"""Module that stores classes with user defined configuration that is mapped
from config.yaml file"""

from dataclasses import field
from typing import Dict, Any, Optional

from pydantic import validator
from pydantic.dataclasses import dataclass


@dataclass(frozen=True)
class ModelConfig:
    """Model training related configuration class
    """

    learning_rate: float
    batch_size: int
    epochs: int
    model_file: str
    epochs_per_checkpoint: int = field(default=5)
    random_seed: Optional[int] = field(default=None)

    @validator("learning_rate")
    def learning_rate_validator(cls, val: float):
        assert val > 0, "Field learning_rate must be higher than 0"
        return val

    @validator("epochs")
    def epochs_validator(cls, val: int):
        assert val > 0, "Field epochs must be higher than 0"
        return val

    @validator("batch_size")
    def batch_size_validator(cls, val: int):
        assert val > 0, "Field batch_size must be higher than 0"
        return val

    @validator("model_file")
    def model_file_validator(cls, val: str):
        assert val != "", "Field output_file must be at least 1 character long"
        return val

    @validator("epochs_per_checkpoint")
    def epochs_per_checkpoint_validator(cls, val: int):
        assert val >= 1, "Field epochs_per_checkpoint must be at least 1 or more"
        return val


@dataclass(frozen=True)
class DatasetConfig:
    """Dataset related configuration class
    """

    root_dir: str
    definition_file: str
    audio_directory: str

    @validator("root_dir")
    def root_dir_validator(cls, val: str):
        assert val != "", "Field root_dir must be at least 1 character long"
        return val

    @validator("definition_file")
    def definition_file_validator(cls, val: str):
        assert val != "", "Field definition_file must be at least 1 character long"
        return val

    @validator("audio_directory")
    def audio_directory_validator(cls, val: str):
        assert val != "", "Field audio_directory must be at least 1 character long"
        return val


@dataclass(frozen=True)
class TrainingConfig:
    """Configuration class for training process.

    Note:
        Configuration classes are strongly typed, all values must match datatypes specified
        with class's fields.
    """

    model: ModelConfig
    dataset: DatasetConfig

    @staticmethod
    def from_dicts(model: Dict[str, Any], dataset: Dict[str, Any]) -> 'TrainingConfig':
        """Creates config object using dictionaries representing model and dataset configuration

        Args:
            model: model configuration representation in dictionary form
            dataset: dataset configuration representation in dictionary form

        Returns:
            TrainingConfig object
        """
        model = ModelConfig(**model)
        dataset = DatasetConfig(**dataset)
        return TrainingConfig(model, dataset)

    @classmethod
    def default(cls) -> 'TrainingConfig':
        """Creates default configuration object

        Returns:
            TrainingConfig object with default settings
        """
        return TrainingConfig(
            model=ModelConfig(
                epochs=500,
                batch_size=80,
                learning_rate=0.001,
                model_file="trained_model.pth"
            ),
            dataset=DatasetConfig(
                root_dir="dataset.zip",
                definition_file="metadata.csv",
                audio_directory="wavs/"
            )
        )
