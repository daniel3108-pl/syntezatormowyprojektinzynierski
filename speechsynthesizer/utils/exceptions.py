"""Module for error classes"""


class ConfigLoadingUnsuccessfulError(Exception):
    """Raised when config loader encountered an illegal situation
    """
    pass


class ZipFileIsNotOpenedError(Exception):
    """Raised when action is invoked on a zipfile that was not yet opened
    """
    pass


class LoggingConfigMissingError(Exception):
    """Raised when LoggerFactory could not load its configuration file
    """
    pass
