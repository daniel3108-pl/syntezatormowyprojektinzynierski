"""Module with utility functions that cannot be categorized in any other way"""

import os.path
from typing import Callable, Any


def is_int(val: str) -> bool:
    """Check if string can be cast to int

    Args:
        val: string

    Returns:
        True if is int, False if not
    """
    if val is None:
        return False
    return _try_cast_to_type(int, val)


def is_float(val: str) -> bool:
    """Check if string can be cast to float

    Args:
        val: string

    Returns:
        True if is float, False if not
    """
    if val is None:
        return False
    return _try_cast_to_type(float, val)


def is_text_empty(text: str) -> bool:
    """Checks if text is None or empty string

    Args:
        text: input text to check

    Returns:
        True if empty, False if not
    """
    return text is None or len(text) < 1


def file_exists(file: str) -> bool:
    """Check if input path is file and it exists

    Args:
        file: file path

    Returns:
        True if valid, False if not
    """
    return os.path.isfile(file) and os.path.exists(file)


def get_script_directory(script_path: str) -> str:
    """Creates and returns absolute path of parent directory of specified script file path

    Args:
        script_path: path of the script specified using __file__

    Returns:
        absolute path of directory that provided script is in
    """
    script_abs_path = os.path.realpath(script_path)
    script_par_dir = os.path.join(script_abs_path, os.pardir)
    return os.path.realpath(script_par_dir)


def _try_cast_to_type(type_caster: Callable[[object], Any], val: str) -> bool:
    try:
        type_caster(val)
        return True
    except ValueError:
        return False
