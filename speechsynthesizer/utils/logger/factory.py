"""Module with classes and functions related to handling logging for application"""

import logging
import logging.config
import os.path
import sys
from typing import Callable

from ..exceptions import LoggingConfigMissingError
from .. import func


def get_logger(name: str) -> logging.Logger:
    """Utility function that creates a logger with LoggerFactory singleton.

    Args:
        name: name for the logger to show in output

    Returns:
        Logger object
    """
    return LoggerFactory.instance().get_logger(name)


class LoggerFactory:
    """Simple logger factory singleton than creates logger objects with loaded configuration
    that is stored in logger.conf file in this package
    """

    _instance: 'LoggerFactory' = None

    def __init__(self):
        """Initializes LoggerFactory singleton, loads config from logger.conf file and
        sets exceptions handler for the application.
        """
        self._load_config()
        sys.excepthook = self._get_exception_handler()  # sets an action when uncaught exception happens

    @classmethod
    def instance(cls) -> 'LoggerFactory':
        """Creates LoggerFactory object if it is not saved in this singleton and then returns it
        or just returns already created one.

        Returns:
            LoggerFactory singleton object
        """
        if cls._instance is None:
            cls._instance = LoggerFactory()
        return cls._instance

    def get_logger(self, name: str) -> logging.Logger:
        """Creates Logger object with specified name and returns it.

        Args:
            name: name for the logger to show

        Returns:
            Logger object
        """
        logger = logging.getLogger(name)
        return logger

    def _load_config(self):
        """Loads logging config
        """
        config_file_path = os.path.join(func.get_script_directory(__file__), "logger.cfg")
        if not os.path.exists(config_file_path):
            raise LoggingConfigMissingError("Logging config file of path: '{config_file_path}' does not exists!")
        logging.config.fileConfig(config_file_path)

    def _get_exception_handler(self) -> Callable:
        """Gets uncaught exception handling function
        """
        return lambda type, value, traceback, logger = self.get_logger("UncaughtExceptionHandler"): \
            logger.error("Uncaught error occurred", exc_info=(type, value, traceback))
