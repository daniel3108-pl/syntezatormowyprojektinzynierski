from typing import Union


def started_learning(epochs: Union[str, int], batch_size: Union[str, int]) -> str:
    """Creates a message that is displayed in logs when learning process is starting

    Args:
        epochs: no. epochs for the training process
        batch_size: batch size configured in yaml file

    Returns:
        message
    """
    return f"Learning process of {epochs} epochs with batch size " + \
           f"{batch_size} has started.\n" + \
           f"Depending of your machine this can take enormous amount of time.\n" + \
           f"Please be patient even if it looks like it is stuck."


def learning_interrupted() -> str:
    """Creates a message that is displayed in logs when learning process was interrupted
    (ctrl + c, invoking sigint signal)

    Returns:
        message
    """
    return "You have interrupted learning process. " + \
           "Model will be saved with current progress, " + \
           "but training report will not be generated if you've chosen do so "
