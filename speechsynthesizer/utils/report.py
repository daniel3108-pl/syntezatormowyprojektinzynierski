import json
import subprocess
import sys
from datetime import datetime
from logging import Logger
from os import path
from typing import Any, Union, List

import fpdf
import numpy as np
from PIL import Image
from fpdf import FPDF
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.figure import Figure
from pydantic.json import pydantic_encoder

from .config import TrainingConfig
from . import logger


class ReportPrinter(FPDF, fpdf.html.HTMLMixin):
    """Creates and then open automatically, PDF report of model's training results
    """

    __FONT: str = "Times New Roman"

    def __init__(self, **params: Any):
        """Initializes ReportPinter object and pdf file layout

        Args:
            **params: dictionary with data to render in pdf
        """
        super(ReportPrinter, self).__init__()
        self.log: Logger = logger.get_logger(self.__class__.__name__)
        self.font_path: str = path.join(path.dirname(__file__), "pdf_fonts")
        self.params = params
        self._add_fonts()
        self._set_metadata()
        self._init_layout()

    def save_report(self, output_path: str):
        """Saves, created by the printer, pdf file and opens automatically it.

        Args:
            output_path: path for pdf file to save to
        """
        self.output(output_path)
        self.log.info(f"Report was generated and saved to the file: '{path.abspath(output_path)}'")
        self._open_pdf(output_path)

    def _add_fonts(self):
        """Adds loaded Times New Roman fonts
        """
        self.add_font(family=self.__FONT, fname=self._get_font("Times New Roman"), uni=True)
        self.add_font(family=self.__FONT, style="B", fname=self._get_font("Times New Roman Bold"), uni=True)
        self.add_font(family=self.__FONT, style="I", fname=self._get_font("Times New Roman Italic"), uni=True)
        self.add_font(family=self.__FONT, style="BI", fname=self._get_font("Times New Roman Bold Italic"), uni=True)

    def _set_metadata(self):
        """Sets report's metadata
        """
        self.set_author("Speech Synthesizer, by: Daniel Swietlik")
        self.set_creation_date(datetime.now())
        self.set_lang("pl")

    def _init_layout(self):
        """Initializes report's layout
        """
        self.log.info(f"Report printing was chosen. Started generating it.")
        self.add_page()
        self.set_margins(20, 20)
        self.set_auto_page_break(True)
        self.set_font(self.__FONT, style="", size=11)
        self._header(self.params.get('dataset'), self.params.get('config_path'))
        self._config(self.params.get('config'))
        self._train_errors()

    def _header(self, dataset: str, config_path: str):
        """Creates a header page for report

        Args:
            dataset: dataset path
            config_path: config file path
        """
        self.empty_line()
        self.empty_line()
        self.h1("Raport trenowania modelu text to speech", align="C")
        self.empty_line()
        self.h2(f"**Data**:  {datetime.now().strftime('%m/%d/%Y, %H:%M:%S')}", align="C", md=True)
        self.h2(f"**Zestaw danych**:  {path.abspath(dataset)}", align="C", md=True)
        self.h2(f"**Konfiguracja**:  {path.abspath(config_path)}", align="C", md=True)
        self.empty_line()
        self.empty_line()

    def _config(self, config: TrainingConfig):
        """Creates a code listing for training configuration in json format

        Args:
            config: config object
        """
        self.h2("1    Wybrana konfiguracja trenowania", style="B")
        self.empty_line(size=5)
        self.add_code_chunk(json.dumps(config, default=pydantic_encoder, indent=3),
                            "Listing 1: Konfiguracja trenowania w formacie JSON")

    def _train_errors(self):
        """Creates a table and plots with training error values
        """
        self.add_page()
        self.h2("2    Błędy w trakcie uczenia", style="B")
        self.print_table(["Epoka", "Błąd zestawu uczenia", "Błąd zestawu walidacyjnego"],
                         self._get_error_rows(),
                         "Tabela 1: Tabela błędów w procesie uczenia")
        x = range(1, self.params.get("tloss").__len__() + 1)
        self.print_plot(x, self.params.get("tloss"), "Błąd trenowania w każdych epokach", "Epoka", "Błąd",
                        "Wykres 1: Błąd trenowania w każdej iteracji")
        self.print_plot(x, self.params.get("vloss"), "Błąd walidacji w każdych epokach", "Epoka", "Błąd",
                        "Wykres 2: Błąd zestawu walidującego w każdej iteracji trenowania", "orange")

    def footer(self):
        """Creates a footer
        """
        self.set_y(-15)
        self.set_font(self.__FONT, size=10)
        self.cell(0, 10, f"{self.page_no()}", 0, 0, "R")

    def empty_line(self, size: int = 10):
        """Creates a empty line of size
        Args:
            size: size of empty space
        """
        self.cell(ln=1, h=size, w=0)

    def h1(self, text: str, align: str = "L", style: str = "B", md: bool = False):
        """Adds bigger header.

        Args:
            text: text of header
            align: alignment
            style: style of text
            md: uses markdown syntax for text
        """
        self.set_font(self.__FONT, style=style, size=16)
        self.cell(txt=text, w=0, ln=1, h=10, align=align, markdown=md)
        self.set_font(self.__FONT, style="", size=11)

    def h2(self, text: str, align: str = "L", style: str = "", md: bool = False):
        """Adds smaller header.

        Args:
            text: text of header
            align: alignment
            style: style of text
            md: uses markdown syntax for text
        """
        self.set_font(self.__FONT, style=style, size=12)
        self.cell(txt=text, w=0, ln=1, h=10, align=align, markdown=md)
        self.set_font(self.__FONT, style="", size=11)

    def add_code_chunk(self, code_text: str, listing: str = ""):
        """Adds a code chunk

        Args:
            code_text: text of code
            listing: text of code chunk description
        """
        self.set_font("Courier", size=8.5)
        self.set_fill_color(247, 247, 247)
        self.set_draw_color(247, 247, 247)
        self.set_line_width(3)
        self.multi_cell(txt=code_text, h=5, ln=1, w=0, fill=True, border=True)
        self.set_line_width(0.2)
        self.set_fill_color(255, 255, 255)
        self.add_description(listing)

    def print_table(self, columns: List[str], data: List[List[Any]], desc: str = ""):
        """Prints a table with provided data and description

        Args:
            columns: names of columns
            data: data to create table from
            desc: description for table
        """
        header_color: str = '"#E0E0E0"'
        cell_color: str = '"#F0F0F0"'
        width: int = 100 // len(columns)
        table_html = f"""<table width=\"100%\" border=\"1\">
        <thead>
            <tr>{[f'<th bgcolor={header_color} width="{width}%">{column}</th>' for column in columns]}</tr>
        </thead>
        <tbody>
        {[f'''
            <tr>{[f"<td {f'bgcolor={cell_color}' if idx % 2 != 0 else ''}>{item}</td>" for item in row]}</tr>
        ''' for idx, row in enumerate(data)]}
        </tbody>
        </table>"""
        self.set_draw_color(0, 0, 0)
        self.write_html(table_html, table_line_separators=False)
        self.add_description(desc)

    def add_description(self, desc: str):
        """Adds description below object

        Args:
            desc: description text
        """
        if desc == "":
            return
        self.set_font(self.__FONT, style="I", size=9)
        self.set_text_color(0, 0, 0)
        self.set_x(self.l_margin)
        self.cell(txt=desc, h=10, ln=1, w=0, align="C")
        self.set_font(self.__FONT, size=11)
        self.empty_line()

    def print_plot(self, x: Union[List[List[Any]], range], y: List[List[Any]], title: str = "x to y",
                   x_label: str = "x", y_label: str = "y", desc: str = "", color: str = "blue"):
        """Creates a plot

        Args:
            x: X axis data
            y: Y axis data
            title: title for the plot
            x_label: label of x axis
            y_label: label of y axis
            desc: description of plot
            color: color of plot line
        """
        img = self._get_plot_image(color, title, x, x_label, y, y_label)
        image_x_pos = self.epw / 1.5 / 2 - self.l_margin / 2
        image_width = self.epw / 1.5
        self.image(img, x=image_x_pos, w=image_width)
        self.add_description(desc)

    @classmethod
    def _get_font(cls, name: str) -> str:
        """Gets a path of specified font

        Args:
            name: font name

        Returns:
            String object with font's path
        """
        if sys.platform == "darwin":
            return f"/System/Library/Fonts/Supplemental/{name}.ttf"
        elif sys.platform == "win32":
            return f"C:\\Windows\\Fonts\\{name}.ttf"
        else:
            return cls._get_linux_font(name)

    @staticmethod
    def _get_linux_font(name: str) -> str:
        """Gets a path of specified font in linux

        Args:
            name: name of the font

        Returns:
            String object with font's path
        """
        if path.exists(f"/usr/share/fonts/{name}.ttf"):
            return f"/usr/share/fonts/{name}.ttf"
        elif path.exists(f"/usr/local/share/fonts/{name}.ttf"):
            return f"/usr/local/share/fonts/{name}.ttf"
        else:
            return f"~/.fonts/{name}.ttf"

    @classmethod
    def _open_pdf(cls, file_path: str):
        """Opens pdf file of specified path

        Args:
            file_path: pdf's path
        """
        pdf_file_path = path.abspath(file_path)
        command = cls._get_open_command()
        subprocess.run([command, pdf_file_path])

    @staticmethod
    def _get_open_command() -> str:
        """Gets open file command for specific operating system
        """
        if sys.platform == "darwin":  # If macOS
            return "open"
        elif sys.platform == "win32":  # If windows
            return "start"
        else:  # If linux (Should work on most desktop environments)
            return "xdg-open"

    def _get_error_rows(self) -> List[List[Union[int, float]]]:
        """Gets error rows for provided loss data

        Returns:
            List of loss data
        """
        rows = []
        losses = zip(self.params.get('tloss', []), self.params.get('vloss', []))
        for i, (train_loss, validation_loss) in enumerate(losses):
            rows.append([i + 1, train_loss, validation_loss])
        return rows

    @classmethod
    def _get_plot_image(cls, color: str, title: str, x: Union[List[List[Any]], range],
                        x_label: str, y: List[List[Any]], y_label: str) -> Image:
        """Gets a Image object for specified plot

        Args:
            color: color of the plot line
            title: title of the plot
            x: x axis data
            x_label: x axis label
            y: y axis data
            y_label: y axis label

        Returns:
            Image object with drawn plot in it
        """
        fig = cls._get_plot_for_printing(color, title, x, x_label, y, y_label)
        canvas = FigureCanvasAgg(fig)
        canvas.draw()
        img = Image.fromarray(np.asarray(canvas.buffer_rgba()))
        return img

    @staticmethod
    def _get_plot_for_printing(color: str, title: str, x: Union[List[List[Any]], range],
                               x_label: str, y: List[List[Any]], y_label: str) -> Figure:
        """Gets a Figure object with line plot

        Args:
            color: color of the plot line
            title: title of the plot
            x: x axis data
            x_label: x axis label
            y: y axis data
            y_label: y axis label

        Returns:
            Figure object of line plot
        """
        fig = Figure((6, 5), dpi=300)
        ax = fig.add_subplot(111)
        ax.set_title(title)
        ax.set_xlabel(x_label)
        ax.set_ylabel(y_label)
        (_,) = ax.plot(x, y, color=color)
        return fig
