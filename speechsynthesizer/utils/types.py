"""Module with custom type annotations"""
from typing import Optional, Literal, Callable

from torch import Tensor

ComputeMode = Optional[Literal["gpu", "cpu"]]
"""Type for compute mode"""
ActivationFunctionType = Optional[Literal["tanh", "relu", ""]]
"""Type for activation function method"""
ActivationFunction = Callable[[Tensor], Tensor]
"""Type for activation function"""
