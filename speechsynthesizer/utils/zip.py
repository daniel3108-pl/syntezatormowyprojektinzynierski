from contextlib import contextmanager
from logging import Logger
from typing import Optional, Union, IO, List
from zipfile import ZipFile

from . import logger
from .exceptions import ZipFileIsNotOpenedError


class ZipFileHandler:
    """Simple class for handling zip files
    """
    _ZIPFILE_IS_NOT_OPENED = "Zip file is not opened"

    def __init__(self):
        """Initialize object and creates logger object
        """
        self.zipfile: Optional[ZipFile] = None
        self.log: Logger = logger.get_logger(self.__class__.__name__)

    @contextmanager
    def open(self, file_path: Union[str, IO[bytes]]) -> 'ZipFileHandler':
        """Opens zip file and closes it automatically using python with-statement

        Args:
            file_path: path to a zip file to open

        Returns:
            a self object with zip opened already

        Examples:

            You use this method as "with-statement"::

                handler = ZipFileHandler()
                with handler.open("some_file.zip") as zip:
                    names = zip.filenames()

        """
        try:
            self.zipfile = ZipFile(file_path)
            yield self
        finally:
            self.close()

    def close(self):
        """Closes, if opened, zip file

        Raises:
            ZipFileIsNotOpenedError: when zip file was not opened
        """
        if not self.zipfile:
            raise ZipFileIsNotOpenedError(self._ZIPFILE_IS_NOT_OPENED)
        self.zipfile.close()

    def get_file(self, filename: str) -> IO[bytes]:
        """Gets a file specified by the name

        Args:
            filename: name for file inside zip
        Returns:
            IO object for the specified file or raises exception
        Raises:
            ZipFileIsNotOpenedError: when zip file was not opened
        """
        if self.zipfile:
            return self.zipfile.open(filename)
        raise ZipFileIsNotOpenedError(self._ZIPFILE_IS_NOT_OPENED)

    def filenames(self) -> List[str]:
        """Return list of files inside zip

        Returns:
            a list of files inside zip
        Raises:
            ZipFileIsNotOpenedError: when zip file was not opened
        """
        if self.zipfile:
            return self.zipfile.namelist()
        raise ZipFileIsNotOpenedError(self._ZIPFILE_IS_NOT_OPENED)
