import os.path
from typing import Any, List
from unittest import TestCase
from unittest import mock

import pydantic
from parameterized import parameterized

from speechsynthesizer.utils import ZipFileHandler
from speechsynthesizer.utils.config import (
    TrainingConfig,
    DatasetConfig,
    ModelConfig,
    ConfigLoader,
    DatasetConfigValidator
)
import test.utils as utils
from speechsynthesizer.utils.exceptions import ConfigLoadingUnsuccessfulError

_METADATA_STR = "metadata.csv"
_WAVS_STR = "wavs/"


class TestTrainingConfiguration(TestCase):

    YAML_FILE_PATH = utils.get_path_from_resources("test_config.yml")
    loader: ConfigLoader = None

    @classmethod
    def setUpClass(cls):
        dataset_validator = cls._create_mock_validator()
        cls.loader = ConfigLoader(dataset_validator)

    def test_should_load_config_correctly(self):

        config: TrainingConfig = self.loader.load(self.YAML_FILE_PATH)

        self.assertEqual("trained_model.pth", config.model.model_file)
        self.assertEqual(0.001, config.model.learning_rate)
        self.assertEqual(None, config.model.random_seed)

        self.assertEqual("./resources/dataset.zip", config.dataset.root_dir)
        self.assertEqual(_METADATA_STR, config.dataset.definition_file)
        self.assertEqual(_WAVS_STR, config.dataset.audio_directory)

    def test_should_raise_error_when_incorrect_input(self):
        self.assertRaises(ConfigLoadingUnsuccessfulError, lambda: self.loader.load("dasdasasddas.asd"))

    def test_should_raise_error_when_not_valid_dataset_config(self):
        dataset_validator = self._create_mock_validator(valid=False)
        loader = ConfigLoader(dataset_validator)
        self.assertRaises(ConfigLoadingUnsuccessfulError, lambda: loader.load(self.YAML_FILE_PATH))

    def test_should_raise_error_when_not_valid_fields(self):
        with mock.patch("speechsynthesizer.utils.config.objs.TrainingConfig.from_dicts",
                        side_effect=TypeError):
            self.assertRaises(ConfigLoadingUnsuccessfulError, lambda: self.loader.load(self.YAML_FILE_PATH))

    @classmethod
    def _create_mock_validator(cls, valid: bool = True) -> DatasetConfigValidator:
        zip_file_handler = ZipFileHandler()
        zip_file_handler.filenames = mock.MagicMock(return_value=[_METADATA_STR, _WAVS_STR])
        zip_file_handler.close = mock.MagicMock(return_value=None)
        dataset_config_validator = DatasetConfigValidator(zip_file_handler)
        dataset_config_validator.validate = mock.MagicMock(return_value=valid)
        return dataset_config_validator


def create_dataset_config(root_dir: str, meta: str = _METADATA_STR, audio: str = _WAVS_STR) -> DatasetConfig:
    return DatasetConfig(
        root_dir=root_dir,
        definition_file=meta,
        audio_directory=audio
    )


class TestDatasetConfigValidator(TestCase):

    CORRECT_ROOT_DIR = utils.get_path_from_resources(os.path.join("datasets", "correct")) + os.path.sep
    INCORRECT_ROOT_DIR = utils.get_path_from_resources(os.path.join("datasets", "incorrect")) + os.path.sep
    ZIP_DATASET = utils.get_path_from_resources(os.path.join("datasets", "dataset.zip"))
    DEFINITION_FILE = _METADATA_STR
    AUDIO_DIR = _WAVS_STR
    validator: DatasetConfigValidator = None

    @classmethod
    def setUpClass(cls):
        cls.validator = cls._create_mock_validator([cls.DEFINITION_FILE, cls.AUDIO_DIR])

    @parameterized.expand([
        (create_dataset_config(CORRECT_ROOT_DIR), True),
        (create_dataset_config(INCORRECT_ROOT_DIR), False),
        (create_dataset_config(ZIP_DATASET), True),
        (create_dataset_config(CORRECT_ROOT_DIR, meta="dasdasdasdasd.csv"), False),
        (create_dataset_config(CORRECT_ROOT_DIR, audio="/dasdasasdasd"), False),
        (create_dataset_config("dasdasasdasd"), False),
        (None, False),
        ([], False),
    ])
    def test_should_validate_correctly(self, correct_config: DatasetConfig, expected: bool):
        result = self.validator.validate(correct_config)
        self.assertEqual(expected, result)

    def test_should_not_validate_with_incorrect_metafile_zip(self):
        dataset_config = create_dataset_config(self.ZIP_DATASET)
        validator = self._create_mock_validator([self.AUDIO_DIR])
        self.assertFalse(validator.validate(dataset_config))

    def test_should_not_validate_with_incorrect_audiodata_zip(self):
        dataset_config = create_dataset_config(self.ZIP_DATASET)
        validator = self._create_mock_validator([self.DEFINITION_FILE])
        self.assertFalse(validator.validate(dataset_config))

    @classmethod
    def _create_mock_validator(cls, values: List[str]):
        zip_handler = ZipFileHandler()
        zip_handler.filenames = mock.MagicMock(return_value=values)
        return DatasetConfigValidator(zip_handler)


class TestModelConfigFieldValidators(TestCase):

    @parameterized.expand([
        ({"learning_rate": 0.001, "batch_size": 1, "epochs": 20, "model_file": "model.pth"}, False),
        ({"learning_rate": -0.001, "batch_size": 1, "epochs": 20, "model_file": "model.pth"}, True),
        ({"learning_rate": "ddd", "batch_size": 1, "epochs": 20, "model_file": "model.pth"}, True),
        ({"learning_rate": 0.001, "batch_size": -1, "epochs": 20, "model_file": "model.pth"}, True),
        ({"learning_rate": 0.001, "batch_size": 1, "epochs": -20, "model_file": "model.pth"}, True),
        ({"learning_rate": 0.001, "batch_size": 1, "epochs": 20, "model_file": ""}, True)
    ])
    def test_should_correctly_validate_fields(self, config_data: dict[str, Any], raises: bool):
        if raises:
            self.assertRaises(pydantic.ValidationError, lambda: ModelConfig(**config_data))
        else:
            ModelConfig(**config_data)


class TestDatasetConfigFieldValidators(TestCase):

    @parameterized.expand([
        ({"root_dir": "as", "definition_file": "dd", "audio_directory": "dd"}, False),
        ({"root_dir": "", "definition_file": "dd", "audio_directory": "dd"}, True),
        ({"root_dir": "as", "definition_file": "", "audio_directory": "dd"}, True),
        ({"root_dir": "as", "definition_file": "dd", "audio_directory": ""}, True),
    ])
    def test_should_correctly_validate_fields(self, config_data: dict[str, Any], raises: bool):
        if raises:
            self.assertRaises(pydantic.ValidationError, lambda: DatasetConfig(**config_data))
        else:
            DatasetConfig(**config_data)
