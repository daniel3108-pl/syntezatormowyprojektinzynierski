import os
from unittest import TestCase

import torch
import torchaudio

from speechsynthesizer.model.utils.mel import WaveToMelConverter, MelToWaveConverter, MelWaveFileWriter

from test.utils import get_path_from_resources


class TestWaveToMelConverter(TestCase):

    WAVE_FILE_NAME = "test-wave.wav"
    converter = WaveToMelConverter()

    def test_should_convert_to_mel_correctly(self):
        test_wave_file = get_path_from_resources(self.WAVE_FILE_NAME)
        waveform, sample_rate = torchaudio.load(test_wave_file)

        mel = self.converter.convert(waveform, sample_rate)

        self.assertEqual((2, 80, 35), tuple(mel.shape))
        self.assertEqual(7645.06005859375, float(mel.sum()))
        self.assertEqual(3.495684102426677e-13, float(mel.min()))
        self.assertEqual(475.8383483886719, float(mel.max()))
        self.assertEqual(1.3651893138885498, float(mel.mean()))
        self.assertEqual(0.0015944630140438676, float(mel.median()))


class TestMelToWaveConverter(TestCase):
    """This test is really just to see an idea of how converter may work as converting wave to mel is lossy,
    so there is no way to convert it and have 100% exact same waveform
    """

    MEL_TENSOR_FILE = "mel-tensor.pt"
    WAVE_SAMPLE_RATE = 44100
    converter = MelToWaveConverter()

    def test_should_convert_to_wave_correctly(self):
        mel_tensor = torch.load(get_path_from_resources(self.MEL_TENSOR_FILE))

        waveform = self.converter.convert(self.WAVE_SAMPLE_RATE, mel_tensor)

        self.assertEqual((2, 8704), tuple(waveform.shape))
        self.assertAlmostEqual(13.415044784545898, float(waveform.sum()), -1)
        self.assertAlmostEqual(-0.9709584712982178, float(waveform.min()), 0)
        self.assertAlmostEqual(0.7750733494758606, float(waveform.max()), 0)
        self.assertAlmostEqual(0.0007706252508796751, float(waveform.mean()), 3)
        self.assertAlmostEqual(0.000816017622128129, float(waveform.median()), 3)


class TestMelWaveFileWriter(TestCase):

    WAVE_FILE_NAME = "test-wave.wav"
    WAVE_OUT_NAME = "out-wave"
    writer = MelWaveFileWriter()

    def test_should_save_wave_file_correctly(self):
        test_wave_file = get_path_from_resources(self.WAVE_FILE_NAME)
        out_wave_file = get_path_from_resources(self.WAVE_OUT_NAME)
        waveform, sample_rate = torchaudio.load(test_wave_file)

        self.writer.write(out_wave_file, sample_rate, waveform)

        act_waveform, act_sample_rate = torchaudio.load(out_wave_file + ".wav")
        self.assertEqual(sample_rate, act_sample_rate)
        self.assertEqual(waveform.tolist(), act_waveform.tolist())

    def test_should_raise_error_when_none_waveform(self):
        self.assertRaises(ValueError, lambda: self.writer.write(None, None, None))

    def tearDown(self):
        out_wave_file = get_path_from_resources(self.WAVE_OUT_NAME + ".wav")
        os.remove(out_wave_file)
