from unittest import TestCase, mock

import torch
from parameterized import parameterized

from speechsynthesizer.model.utils.func import get_torch_device, cat_tensor_unsqueeze, new_tensor_variable
from speechsynthesizer.utils import is_int, is_float
from speechsynthesizer.utils.func import is_text_empty, file_exists
from test import utils


class TestOtherUtilsMethods(TestCase):

    TEST_FILE_PATH = utils.get_path_from_resources("test_config.yml")

    @parameterized.expand([
        ("asd", False),
        ("2", True),
        ("2a", False),
        ("a2", False),
        ("", False),
        (None, False)
    ])
    def test_is_int(self, text: str, expected: bool):
        actual: bool = is_int(text)
        self.assertEqual(expected, actual)

    @parameterized.expand([
        ("asd", False),
        ("2.0", True),
        ("2.", True),
        ("2.a", False),
        ("2.0a", False),
        ("a2.0", False),
        ("", False),
        (None, False)
    ])
    def test_is_float(self, text: str, expected: bool):
        actual: bool = is_float(text)
        self.assertEqual(expected, actual)

    @parameterized.expand([
        ("", True),
        (None, True),
        ("1323123123123", False)
    ])
    def test_is_text_empty(self, text: str, expected: bool):
        actual: bool = is_text_empty(text)
        self.assertEqual(expected, actual)

    @parameterized.expand([
        ("dasdasdas.pl", False),
        ("", False),
        ("dasdasdasd", False),
        (TEST_FILE_PATH, True)
    ])
    def test_file_exists(self, file: str, expected: bool):
        actual: bool = file_exists(file)
        self.assertEqual(expected, actual)

    def test_get_torch_device_should_return_cpu_if_no_cuda_but_use_gpu(self):
        with mock.patch("torch.cuda.is_available", return_value=False):
            actual: str = get_torch_device("gpu")
            self.assertEqual(torch.device("cpu"), actual)

    def test_get_torch_device_should_return_cuda_if_available_and_gpu(self):
        with mock.patch("torch.cuda.is_available", return_value=True):
            actual: str = get_torch_device("gpu")
            self.assertEqual(torch.device("cuda"), actual)

    def test_get_torch_device_should_return_cpu_if_mode_cpu(self):
        actual: str = get_torch_device("cpu")
        self.assertEqual(torch.device("cpu"), actual)

    def test_cat_tensor_unsqueeze(self):
        t1 = torch.zeros(2)
        t2 = torch.zeros(2)
        actual: torch.Tensor = cat_tensor_unsqueeze(t1, t2)
        self.assertEqual(torch.Size([2, 2]), actual.size())

    def test_new_tensor_variable(self):
        actual: torch.autograd.Variable = new_tensor_variable((2, 2))
        self.assertEqual(torch.Size([2, 2]), actual.size())
        self.assertIsInstance(actual, torch.autograd.Variable)
