from typing import List
from unittest import TestCase

import torch
from parameterized import parameterized

from speechsynthesizer.model.utils.text import TextEncoder, TextDecoder, TextCleaner


class TestTextEncoder(TestCase):

    text_encoder = TextEncoder()

    @parameterized.expand([
        ("", None),
        (None, None),
        ("Sample speech text", torch.tensor([30, 12, 24, 27, 23, 16, 11, 30, 27, 16, 16, 14, 19, 11, 31, 16, 35, 31])),
        ("This is 32another one", torch.tensor([31, 19, 20, 30, 11, 20, 30, 11, 12, 25, 26, 31, 19, 16, 29, 11, 26, 25, 16])),
    ])
    def test_should_encode_text_correctly(self, input_text: str, expected: torch.Tensor):
        actual = self.text_encoder.encode(input_text)
        self._assert_tensors_equals(actual, expected)

    def _assert_tensors_equals(self, actual, expected):
        result = actual == expected
        if isinstance(result, torch.Tensor):
            self.assertTrue(all(result))
        else:
            self.assertTrue(result)


class TestTextDecoder(TestCase):

    text_decoder = TextDecoder()

    @parameterized.expand([
        ([], ""),
        (None, ""),
        ([30, 12, 24, 27, 23, 16, 11, 30, 27, 16, 16, 14, 19, 11, 31, 16, 35, 31], "sample speech text"),
        ([31, 19, 20, 30, 11, 20, 30, 11, 12, 25, 26, 31, 19, 16, 29, 11, 26, 25, 16], "this is another one")
    ])
    def test_should_decode_sequence_correctly(self, input: List, expected: str):
        actual = self.text_decoder.decode(input)
        self.assertEqual(expected, actual)


class TestTextCleaner(TestCase):

    text_cleaner = TextCleaner()

    @parameterized.expand([
        ("", ""),
        (None, ""),
        ("tesTing+32, ", "testing, "),
        ("            ", " "),
        ("AsAdSdS:_@^", "asadsds:_"),
        ("mr.", "mister")
    ])
    def test_should_clean_text_correctly(self, input: str, expected: str):
        actual = self.text_cleaner.clean(input)
        self.assertEqual(expected, actual)