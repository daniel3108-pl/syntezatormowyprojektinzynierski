import os
from unittest import TestCase

from speechsynthesizer.utils import ZipFileHandler, ZipFileIsNotOpenedError
from test import utils


class TestZipFileHandler(TestCase):

    ZIP_FILE = utils.get_path_from_resources(os.path.join("datasets", "dataset.zip"))

    def test_should_raise_when_closing_closed_zip(self):
        handler = ZipFileHandler()
        self.assertRaises(ZipFileIsNotOpenedError, lambda: handler.close())

    def test_should_raise_when_get_file_from_closed_zip(self):
        handler = ZipFileHandler()
        self.assertRaises(ZipFileIsNotOpenedError, lambda: handler.get_file("dasdasasd"))

    def test_should_raise_when_get_filenames_from_closed_zip(self):
        handler = ZipFileHandler()
        self.assertRaises(ZipFileIsNotOpenedError, lambda: handler.filenames())

    def test_should_return_correct_filenames(self):
        handler = ZipFileHandler()
        with handler.open(self.ZIP_FILE) as zip:
            self.assertEquals(["dataset.txt"], zip.filenames())

    def test_should_return_correct_file(self):
        handler = ZipFileHandler()
        with handler.open(self.ZIP_FILE) as zip:
            dataset_file = zip.get_file("dataset.txt")
            self.assertEqual("dataset.txt", dataset_file.name)
            self.assertFalse(dataset_file.closed)
