import os.path


def get_path_from_resources(name: str) -> str:
    resource_rel_path = os.path.join(_get_script_directory(__file__), "resources", name)
    return os.path.realpath(resource_rel_path)


def _get_script_directory(script_path: str) -> str:
    script_abs_path = os.path.realpath(script_path)
    script_par_dir = os.path.join(script_abs_path, os.pardir)
    return os.path.realpath(script_par_dir)
